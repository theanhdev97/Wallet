import { AppRegistry } from "react-native";
import React, {Component} from 'react';
import App from "./src/containers/app/index";
import { Provider } from "react-redux";
import store from "./src/store/index";

const Index = () => (
	<Provider store={store}>
		<App />
	</Provider>
);

AppRegistry.registerComponent("app_wallet", () => Index);
