import MTransaction from "./MTransaction";
import MCoin from "./MCoin.js";

export default class Data {
	static sPortfolioBalance = "300,000,000";

	static sListNatoTransactions = [];
	static sNato = new MCoin(
		"NATO",
		100000,
		1.105,
		"1,520,452",
		3.5,
		"NATO",
		"blue",
		0.01
	);
	// static sNatoBalance = 100;
	// static sNatoRate = 1.105;
	// static sNatoVolume = "1,520,452";
	// static sNatoOrientation = 3.5;

	static sListUSDTransactions = [];
	// static sUSDBalance = 120;
	// static sUSDRate = 1.0;
	// static sUSDVolume = "11,320,932";
	// static sUSDOrientation = 0.5;
	static sUSD = new MCoin(
		"USD",
		40230,
		1.0,
		"11,320,932",
		0.5,
		"USD",
		"#21ADE7",
		0.01
	);

	static sListSGDTransactions = [];
	// static sSGDBalance = 1000;
	// static sSGDRate = 0.435;
	// static sSGDVolume = "524,322";
	// static sSGDOrientation = 0.8;
	static sSGD = new MCoin(
		"SGD",
		65240,
		0.435,
		"524,322",
		0.8,
		"SGD",
		"#71AA3F",
		0.01
	);

	static sListBitcoinTransactions = [];
	// static sBitcoinBalance = 400;
	// static sBitcoinRate = 5920.33;
	// static sBitcoinVolume = "100,226,442";
	// static sBitcoinOrientation = 2.4;
	static sBitcoin = new MCoin(
		"BITCOIN",
		30,
		5920.33,
		"100,226,442",
		2.4,
		"BTC",
		"orange",
		0.01
	);

	static sListEthereumTransactions = [];
	// static sEthereumBalance = 352;
	// static sEthereumRate = 540.23;
	// static sEthereumVolume = "50,228,992";
	// static sEthereumOrientation = 2.1;
	static sEthereum = new MCoin(
		"ETHEREUM",
		352,
		540.23,
		"50,228,992",
		2.1,
		"ETH",
		"red",
		0.01
	);

	static sListXRPTransactions = [];
	// static sXRPBalance = 120;
	// static sXRPRate = 2.45;
	// static sXRPVolume = "31,523,752";
	// static sXRPOrientation = 1.9;
	static sXRP = new MCoin(
		"XRP",
		20000,
		2.45,
		"31,523,752",
		1.9,
		"XRP",
		"#DF4042",
		0.01
	);

	static initBitcoinTransactions() {
		Data.sListBitcoinTransactions.push(
			new MTransaction(
				"BTC",
				"31-01-2018",
				"RECEIVED",
				"120.02",
				"0x50123f651298ff160b438320e09fe58c513ba56c"
			)
		);

		Data.sListBitcoinTransactions.push(
			new MTransaction(
				"BTC",
				"30-01-2018",
				"RECEIVED",
				"230.02",
				"0x21323f651298ff160b438320e09fe58c513ba34s"
			)
		);

		Data.sListBitcoinTransactions.push(
			new MTransaction(
				"BTC",
				"30-01-2018",
				"SEND",
				"1233.02",
				"0x21323f651298ff160b438320e09fe58c513ba34s"
			)
		);
	}
	static initEthereumTransactions() {
		Data.sListEthereumTransactions.push(
			new MTransaction(
				"ETH",
				"20-01-2018",
				"SEND",
				"4.22",
				"0x50123f651298ff160b438320e09fe58c513ba56c"
			)
		);

		Data.sListEthereumTransactions.push(
			new MTransaction(
				"ETH",
				"15-01-2018",
				"RECEIVED",
				"10.02",
				"0x21323f651298ff160b438320e09fe58c513ba34s"
			)
		);

		Data.sListEthereumTransactions.push(
			new MTransaction(
				"ETH",
				"18-01-2018",
				"SEND",
				"33.02",
				"0x21323f651298ff160b438320e09fe58c513ba34s"
			)
		);
	}
	static initNatoTransactions() {
		Data.sListNatoTransactions.push(
			new MTransaction(
				"NATO",
				"28-01-2018",
				"RECEIVED",
				"23.33",
				"0x50123f651298ff160b438320e09fe58c513ba56c"
			)
		);

		Data.sListNatoTransactions.push(
			new MTransaction(
				"NATO",
				"17-01-2018",
				"RECEIVED",
				"100.02",
				"0x21323f651298ff160b438320e09fe58c513ba34s"
			)
		);

		Data.sListNatoTransactions.push(
			new MTransaction(
				"NATO",
				"16-01-2018",
				"RECEIVED",
				"330.02",
				"0x21323f651298ff160b438320e09fe58c513ba34s"
			)
		);
	}

	static initSGDTransactions() {
		Data.sListSGDTransactions.push(
			new MTransaction(
				"SGD",
				"28-01-2018",
				"RECEIVED",
				"23.33",
				"0x50123f651298ff160b438320e09fe58c513ba56c"
			)
		);

		Data.sListSGDTransactions.push(
			new MTransaction(
				"SGD",
				"17-01-2018",
				"RECEIVED",
				"100.02",
				"0x21323f651298ff160b438320e09fe58c513ba34s"
			)
		);

		Data.sListSGDTransactions.push(
			new MTransaction(
				"SGD",
				"16-01-2018",
				"RECEIVED",
				"330.02",
				"0x21323f651298ff160b438320e09fe58c513ba34s"
			)
		);
	}

	static initXRPTransactions() {
		Data.sListXRPTransactions.push(
			new MTransaction(
				"XRP",
				"28-01-2018",
				"RECEIVED",
				"23.33",
				"0x50123f651298ff160b438320e09fe58c513ba56c"
			)
		);

		Data.sListXRPTransactions.push(
			new MTransaction(
				"XRP",
				"17-01-2018",
				"RECEIVED",
				"100.02",
				"0x21323f651298ff160b438320e09fe58c513ba34s"
			)
		);

		Data.sListXRPTransactions.push(
			new MTransaction(
				"XRP",
				"16-01-2018",
				"RECEIVED",
				"330.02",
				"0x21323f651298ff160b438320e09fe58c513ba34s"
			)
		);
	}

	static initUSDTransactions() {
		Data.sListUSDTransactions.push(
			new MTransaction(
				"USD",
				"28-01-2018",
				"RECEIVED",
				"23.33",
				"0x50123f651298ff160b438320e09fe58c513ba56c"
			)
		);

		Data.sListUSDTransactions.push(
			new MTransaction(
				"USD",
				"17-01-2018",
				"RECEIVED",
				"100.02",
				"0x21323f651298ff160b438320e09fe58c513ba34s"
			)
		);

		Data.sListUSDTransactions.push(
			new MTransaction(
				"USD",
				"16-01-2018",
				"RECEIVED",
				"330.02",
				"0x21323f651298ff160b438320e09fe58c513ba34s"
			)
		);
	}
}
