export default class MCoin {
	constructor(_name, _balance, _rate, _volume, _orientation, _type, _color,_fee) {
		// this.name = _name;
		this.name = _name;
		this.balance = _balance;
		this.rate = _rate;
		this.volume = _volume;
		this.orientation = _orientation;
		this.type = _type;
		this.color = _color;
		this.fee = _fee;
	}
}
