export default class MTransaction {
	constructor(_coinType, _time, _money, _fromAddress, _toAddress,_timeMilis) {
		this.coinType = _coinType;
		this.time = _time;
		this.money = _money;
		this.fromAddress = _fromAddress;
		this.toAddress = _toAddress;
		this.timeMilis = _timeMilis;
	}
}
