import { StyleSheet } from "react-native";

const style = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center"
	},
	image_logo: {
		resizeMode: "cover",
		width: 200,
		height: 200
	},

	text_title: {
		color: "orange",
		fontSize: 30,
		fontWeight: "bold",
		marginTop: 10
	}
});

export default style;
