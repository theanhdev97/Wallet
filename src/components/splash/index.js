import React, { PureComponent } from "react";
import { View, Text, Image, Animated, ImageBackground } from "react-native";
import {Actions} from 'react-native-router-flux'
import style from "./style";

let IC_LOGO = require("../../../assets/image/ic_logo.png");

export default class Splash extends PureComponent {
	componentDidMount() {
		this.timeout = setTimeout(() => {
			Actions.Home();
		}, 3000);
	}

	componentWillUmount() {
		this.clearTimeout(this.timeout);
	}

	render() {
		return (
			<ImageBackground
				source={require("../../../assets/image/background_splash.png")}
				style={{ width: "100%", height: "100%" }}
			/>
		);
	}
}
