import React, { PureComponent } from "react";
import { View, Text, TouchableOpacity, Platform } from "react-native";
import { Button } from "native-base";
import Spinner from "react-native-loading-spinner-overlay";

class CustomAlert extends PureComponent {
	render() {
		return (
			<Spinner visible={this.props.visible} cancelable={false}>
				<View
					style={{
						flex: 1,
						justifyContent: "center",
						alignItems: "center"
					}}
				>
					<View
						style={{
							backgroundColor: "white",
							height: Platform.select({ android: 150, ios: 130 }),
							width: 300,
							borderRadius: 15,
							borderWidth: 5,
							borderColor: "white",
							alignItems: "center"
						}}
					>
						<Text
							style={{
								marginTop: 15,
								fontSize: 20,
								fontWeight: "bold"
							}}
						>
							{this.props.title}
						</Text>
						<Text style={{ color: "black", marginTop: 5 }}>
							{this.props.message}
						</Text>
						<View
							style={{
								marginTop: 15,
								marginBottom: 15,
								height: 0.5,
								width: "100%",
								backgroundColor: "gray",
								jusitfyContent: "center"
							}}
						/>
						<TouchableOpacity
							style={{
								flex: 1,
								alignItems: "center",
								// backgroundColor: "pink"
							}}
							onPress={this.props.onPressOK}
						>
							<Text
								style={{
									width: 250,
									textAlign: "center",
									color: "#1464FA",
									fontWeight: "bold",
									fontSize: 20,
									// marginBottom: Platform.select({
									// 	android: 10,
									// 	ios: 0
									// })
								}}
							>
								OK
							</Text>
						</TouchableOpacity>
					</View>
				</View>
			</Spinner>
		);
	}
}

export default CustomAlert;
