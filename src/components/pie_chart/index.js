import React from "react";
import { PieChart } from "react-native-svg-charts";

class PieChartExample extends React.PureComponent {
    render() {
        let { colors, percents } = this.props;

        const pieData = percents.map((value, index) => ({
            value,
            svg: {
                fill: colors[index],
                onPress: () => console.log("press", index)
            },
            key: `pie-${index}`
        }));

        return <PieChart style={{ height: 200 }} data={pieData} />;
    }
}

export default PieChartExample;
