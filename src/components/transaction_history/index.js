import React, { Component, PureComponent } from "react";
import {
	FlatList,
	Dimensions,
	View,
	Alert,
	Image,
	ActivityIndicator,
	Platformsss
} from "react-native";
import {
	Text,
	Container,
	Title,
	Body,
	Content,
	Button,
	List,
	ListItem,
	Left,
	Right,
	ActionSheet,
	Accordion
} from "native-base";
import { Icon } from "react-native-elements";
import style from "./style";
import MTransaction from "../../model/MTransaction";
import LoadingView from "../loading_view/index";
import Data from "../../model/data";
import FireBase from "../../api/firebase";

let TYPE_SEND = "SEND";
let TYPE_RECEIVED = "RECEIVED";

export default class TransactionHistory extends Component {
	constructor(props) {
		super(props);
		this.state = {
			listDatas: [],
			refreshing: false
		};
		this.nextTypeCoin = props.typeCoin;
		this.mListDatas = [];
	}

	componentDidMount() {
		this._fetchDataWithPage(1);
	}

	shouldComponentUpdate(nextProps, nextState) {
		if (
			this.state.listDatas != nextState.listDatas ||
			this.state.refreshing != nextState.refreshing
		) {
			return true;
		}
		return false;
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.typeCoin != nextProps.typeCoin) {
			this.nextTypeCoin = nextProps.typeCoin;
			this._handleOnRefresh();
		}
	}

	_fetchDataWithPage(page) {
		if (page == 1) {
			this.setState({
				refreshing: true
			});
			FireBase.getTransactionByType(
				this.nextTypeCoin,
				this._handleOnFetchDataSuccess
			);
		}
	}

	_handleOnEmptyData = () => {};

	_handleOnFetchDataSuccess = data => {
		// if (Platform.OS == "android") data.reverse();

		data.sort(function(a, b) {
			return b.timeMilis - a.timeMilis;
		});

		for (let i = 0; i < data.length; i++) {
			console.log(
				"Time : " + data[i].timeMilis + " - Money : " + data[i].money
			);
		}

		this.mListDatas.length = 0;
		this.mListDatas.push(...data);

		this.setState({
			listDatas: data,
			refreshing: false
		});
	};

	_handleOnRefresh = () => {
		this.setState({
			refreshing: true,
			listDatas: []
		});
		this._fetchDataWithPage(1);
	};

	_handleOnFetchDataFailure = () => {};

	_checkTransactionType = transaction => {
		if (
			transaction.fromAddress ==
				FireBase.ACCOUNT_INFO().CURRENT_ADDRESS ||
			transaction.fromAddress ==
				FireBase.ACCOUNT_INFO().CURRENT_ADDRESS_1 ||
			transaction.fromAddress ==
				FireBase.ACCOUNT_INFO().CURRENT_ADDRESS_2 ||
			transaction.fromAddress ==
				FireBase.ACCOUNT_INFO().CURRENT_ADDRESS_3 ||
			transaction.fromAddress == FireBase.ACCOUNT_INFO().CURRENT_ADDRESS_4
		)
			return TYPE_SEND;
		return TYPE_RECEIVED;
	};

	render() {
		let { refreshing } = this.state;
		return (
			<FlatList
				onRefresh={this._handleOnRefresh}
				data={this.state.listDatas}
				extraData={this.state}
				refreshing={false}
				keyExtractor={item => item.money + item.time}
				renderItem={({ item }) => this._renderItem(item)}
				ListHeaderComponent={this._renderHeaderLoadMore}
			/>
		);
	}

	_renderHeaderLoadMore = () => {
		if (this.state.refreshing) return <LoadingView />;
		return null;
	};

	_renderFooterLoadMore = () => (
		<View>
			<ActivityIndicator size="large" color="orange" />
		</View>
	);

	_renderLoadingShimmer = () => (
		<ShimmerPlaceHolder autoRun width={200} height={30} />
	);

	_renderItem = item => (
		<ListItem>
			<Body>
				<Text
					style={{
						color: "gray",
						fontSize: 16
					}}
				>
					{item.time}
				</Text>

				{this._checkTransactionType(item) == TYPE_RECEIVED ? (
					<Text
						style={{
							color: "green",
							fontSize: 20,
							marginTop: 5
						}}
					>
						{TYPE_RECEIVED}
					</Text>
				) : (
					<Text
						style={{
							color: "red",
							fontSize: 20,
							marginTop: 5
						}}
					>
						{TYPE_SEND}
					</Text>
				)}
				<View
					style={{
						flexDirection: "row",
						marginTop: 5
					}}
				>
					<Text
						style={{
							color: "#379BF4",
							fontSize: 12.5
						}}
					>
						{this._checkTransactionType(item) == TYPE_RECEIVED
							? "FROM : "
							: "TO : "}
					</Text>
					<Text style={{ fontSize: 12.5, marginLeft: -5,marginBottom:10 }}>
						{this._checkTransactionType(item) == TYPE_RECEIVED
							? item.fromAddress
							: item.toAddress}
					</Text>
				</View>
				{this._checkTransactionType(item) == TYPE_RECEIVED ? (
					<Button
						success
						style={{
							position: "absolute",
							// top: 10,
							right: 10,
							width: 150,
							justifyContent: "center"
						}}
					>
						<Text style={{ fontSize: 14 }}>
							{parseFloat(item.money).toFixed(2)} {item.coinType}
						</Text>
					</Button>
				) : (
					<Button
						danger
						style={{
							// padding: 10,
							position: "absolute",
							// top: 10,
							right: 10,
							width: 150,
							justifyContent: "center"
						}}
					>
						<Text style={{ fontSize: 14 }}>
							{item.money} {item.coinType}
						</Text>
					</Button>
				)}
			</Body>
		</ListItem>
	);
}
