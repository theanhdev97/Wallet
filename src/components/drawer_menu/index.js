import React, { PureComponent } from "react";
import { View, Image, ImageBackground, Platform } from "react-native";
import { List, Left, Body, Text, Thumbnail } from "native-base";
import { ListItem } from "react-native-paper";
import { Icon } from "react-native-elements";
import { MENUS } from "../../utils/constants";
import style from "./style";
import { Actions } from "react-native-router-flux";

import FireBase from "../../api/firebase";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Action from "../../actions"; //Import your actions

let IC_LOGO = require("../../../assets/image/ic_logo_drawer_menu.png");
let BACKGROUND = require("../../../assets/image/header_drawer_menu.jpg");

class DrawerMenu extends PureComponent {
	_hanleOnPressMenuItem = item => {
		switch (item) {
			case "Log out":
				Actions.Login();
				break;
			default:
				alert("This feature is comming soon");
				break;
		}
	};

	render() {
		return (
			<View style={style.container}>
				{this._renderHeader()}
				<View style={{ backgroundColor: "white", flex: 1 }}>
					<View style={style.flatlistContainer}>
						{this._renderFlatListMenu()}
					</View>
				</View>
				{this._renderCopyright()}
			</View>
		);
	}

	// _renderHeader = () => (
	// 	<ImageBackground
	// 		source={BACKGROUND}
	// 		style={{
	// 			width: "100%",
	// 			alignItems: "center",
	// 			paddingTop: 40,
	// 			paddingBottom: 20
	// 		}}
	// 	>
	// 		<Image
	// 			source={require("../../../assets/image/background_header_drawer_menu.png")}
	// 			style={{ width: 230, height: 30 }}
	// 			resizeMode="stretch"
	// 		/>
	// 	</ImageBackground>
	// );

	_renderHeader = () => (
		<View
			style={{
				width: "100%",
				backgroundColor: "#3366CC",
				alignItems: "center",
				justifyContent: "center",
				height: Platform.select({ android: 56, ios: 60 })
			}}
		>
			<Image
				source={require("../../../assets/image/background_header_drawer_menu.png")}
				style={{ width: 230, height: 30 }}
				resizeMode="stretch"
			/>
		</View>
	);

	_renderFlatListMenu = () => (
		<List
			dataArray={MENUS}
			renderRow={item => this._renderMenuItem(item)}
		/>
	);

	_renderMenuItem = item => (
		<View>
			<ListItem
				onPress={() => this._hanleOnPressMenuItem(item.name)}
				title={<Text style={style.menuItemTitle}>{item.name}</Text>}
				icon={
					<Icon
						color="gray"
						name={item.icon.name}
						type={item.icon.type}
						size={25}
					/>
				}
			/>
		</View>
	);

	_renderCopyright = () => (
		<View
			style={{
				flex: 1,
				backgroundColor: "white",
				width: "100%",
				position: "absolute",
				bottom: 10,
				left: 0,
				alignItems: "center",
				justifyContent: "center",
				flexDirection: "row"
			}}
		>
			<Icon name="copyright" size={15} type="material-community" />
			<Text style={{ textAlign: "center", padding: 8, fontSize: 14 }}>
				2018 - Powered by Nato Exchange
			</Text>
		</View>
	);
}

function mapStateToProps(state, props) {
	return {
		totalBalance: state.coinReducer.totalBalance
	};
}
export default connect(
	mapStateToProps,
	null
)(DrawerMenu);
