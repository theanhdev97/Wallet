import { StyleSheet } from "react-native";
import { Platform } from "react-native";

const style = StyleSheet.create({
	container: {
		// backgroundColor: "pink",
		height: "100%",
		width: "100%"
	},
	headerContainer: {
		flexDirection: "row",
		paddingTop: 60,
		paddingBottom: 60,
		paddingLeft: 30,
		justifyContent: "center",
		paddingRight: 30
	},
	headerIcon: {
		// height: Platform.select({ android: 10, ios: 60 }),
		height: 60,
		width: "100%",
		resizeMode: "stretch"
	},
	headerTitle: {
		padding: 10,
		fontSize: 30,
		fontWeight: "900",
		letterSpacing: 2,
		color: "#FFFDE7"
	},
	menuItemTitle: {
		fontSize: 20
		// letterSpacing: 1.1
	},
	flatlistContainer: {
		// height: "100%",
		backgroundColor: "white"
	}
});

export default style;
