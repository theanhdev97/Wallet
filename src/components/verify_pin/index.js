import React, { Component } from "react";
import {
	FlatList,
	Dimensions,
	Text,
	View,
	Image,
	ScrollView,
	RefreshControl,
	TextInput,
	ImageBackground
} from "react-native";
import {
	Card,
	Container,
	CardItem,
	Title,
	Body,
	Content,
	Left,
	Button,
	Right
} from "native-base";
import { Icon } from "react-native-elements";
// import CodePin from "react-native-pin-code";
import { Actions } from "react-native-router-flux";

import Spinner from "react-native-loading-spinner-overlay";
var Spinkit = require("react-native-spinkit");

let IC_LOGO = require("../../../assets/image/ic_logo.png");
let BACKGROUND = require("../../../assets/image/background_verify_pin.png");

var context;

import FireBase from "../../api/firebase";

export default class VerifyPin extends Component {
	static verifyPinForAccTest() {
		context.mPin = "2606";
		context._submitPin();
	}

	constructor(props) {
		super(props);
		context = this;
		this.state = {
			selectedPin: [false, false, false, false],
			submitting: false,
			showKeyboard: true
		};
		this.mPin = "";
		this.mCurrentIndex = 0;
	}

	componentWillUdpate(nextProps, nextState) {
		if (
			this.state.pin != nextState.pin ||
			this.state.selectedPin != nextState.selectedPin ||
			this.state.showKeyboard != nextState.showKeyboard ||
			this.state.submitting != nextState.submitting
		) {
			return true;
		}
		return false;
	}

	_handleOnChangePin = text => {
		this.mPin = this.mPin + text[text.length - 1];
		console.log("PIN : " + this.mPin);
		let { selectedPin } = this.state;
		selectedPin[this.mCurrentIndex] = true;
		this.mCurrentIndex++;
		this.setState({
			selectedPin: selectedPin
		});

		if (this.mPin.length == 4) {
			this._submitPin();
		}
	};

	_submitPin = () => {
		this.setState({
			submitting: true
		});
		this._verifyPin();
	};

	_verifyPin = () => {
		setTimeout(() => {
			let isSuccess = false;
			this.setState({ submitting: false, showKeyboard: false });
			if (
				FireBase.INDEX == 0 ||
				FireBase.INDEX == 3 ||
				FireBase.INDEX == 4
			) {
				if (this.mPin == "2606") isSuccess = true;
				else isSuccess = false;
			}

			if (FireBase.INDEX == 1) {
				if (this.mPin == "2940") isSuccess = true;
				else isSuccess = false;
			}

			if (FireBase.INDEX == 2) {
				if (this.mPin == "3586") isSuccess = true;
				else isSuccess = false;
			}

			if (isSuccess) Actions.Home();
			else this._resetPin();
		}, 2000);
	};

	_resetPin = () => {
		this.mCurrentIndex = 0;
		this.mPin = "";
		setTimeout(() => {
			this.setState({
				selectedPin: [false, false, false, false],
				showKeyboard: true
			});
		}, 100);
	};

	render() {
		return (
			<ImageBackground style={{ flex: 1 }} source={BACKGROUND}>
				{this._renderSendingOverlay()}
				<View style={{ marginTop: 100, alignItems: "center" }}>
					<Image
						source={IC_LOGO}
						style={{ width: 100, height: 100 }}
						resizeMode="stretch"
					/>
				</View>
				<View style={{ marginTop: 60, alignItems: "center" }}>
					<Text style={{ fontSize: 25, color: "orange" }}>
						Enter Pin
					</Text>
				</View>
				<View
					style={{
						position: "absolute",
						bottom: 0,
						alignItems: "center"
					}}
				>
					{this.state.showKeyboard && (
						<TextInput
							ref={ref => (this.inputPin = ref)}
							style={{
								color: "transparent",
								position: "absolute",
								bottom: 0
							}}
							keyboardType={"numeric"}
							autoFocus
							value={""}
							onChangeText={pin => this._handleOnChangePin(pin)}
						/>
					)}
				</View>
				<View
					style={{
						flexDirection: "row",
						justifyContent: "center",
						margin: 30
					}}
				>
					{this._renderFourDotIcon()}
				</View>
			</ImageBackground>
		);
	}

	_renderFourDotIcon = () => {
		let arr = [];
		let { selectedPin } = this.state;
		selectedPin.map((item, index) => {
			let marginRight = 0;
			if (index != 3) marginRight = 20;
			arr.push(
				<View key={index} style={{ marginRight: marginRight }}>
					<Icon
						type="material-community"
						name={"circle"}
						color={item == true ? "#3366CC" : "gray"}
						size={30}
					/>
				</View>
			);
		});
		return arr;
	};

	_renderSendingOverlay = () => (
		<Spinner visible={this.state.submitting} cancelable={false}>
			<View
				style={{
					flex: 1,
					justifyContent: "center",
					alignItems: "center"
				}}
			>
				<View
					style={{
						backgroundColor: "white",
						justifyContent: "center",
						alignItems: "center",
						width: 300,
						height: 200,
						borderRadius: 5,
						borderWidth: 1,
						borderColor: "black"
					}}
				>
					<Spinkit
						isVisible={true}
						color="orange"
						size={60}
						type={"CircleFlip"}
					/>
					<Text
						style={{
							marginTop: 10,
							fontSize: 30,
							color: "orange"
						}}
					>
						Verifying
					</Text>
				</View>
			</View>
		</Spinner>
	);
}
