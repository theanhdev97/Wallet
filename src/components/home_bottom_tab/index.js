import BottomNavigation, {
	ShiftingTab
} from "react-native-material-bottom-navigation";

import React, { Component } from "react";
import { View } from "react-native";
import { Icon } from "react-native-elements";
import { Tabs, Tab, TabHeading, Text } from "native-base";

import { BOTTOM_TAB } from "../../utils/constants";
import DashboardTab from "../../containers/dashboard_tab/index";
import ExchangeTab from "../../containers/exchange_tab/index";
import RequestTab from "../../containers/request_tab/index";
import SendTab from "../../containers/send_tab/index";

// REDUX
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Action from "../../actions"; //Import your actions

const TAB_DASHBOARD_INDEX = 0;
const TAB_SEND_INDEX = 1;
const TAB_EXCHANGE_INDEX = 2;
const TAB_REQUEST_INDEX = 3;

let COLOR_TAB_UNACTIVE = "gray";
// let COLOR_TAB_ACTIVE = "#FFD600";
let COLOR_TAB_ACTIVE = "#3366CC";
let ICON_SIZE_UNACTIVE = 25;
let ICON_SIZE_ACTIVE = 35;

class HomeBottomTab extends Component {
	constructor(props) {
		super(props);
		this.state = {
			activeTabIndex: 0
		};
		this._handleOnChangeTab = this._handleOnChangeTab.bind(this);
	}

	componentWillUnmount() {
		this.setState({
			activeTabIndex: undefined
		});
		this._handleOnChangeTab = undefined;
	}

	_handleOnChangeTab(tab) {
		let index = tab.i;
		let title = "";
		this.setState({
			activeTabIndex: index
		});
		switch (index) {
			case 0:
				title = "Dashboard";
				break;
			case 1:
				title = "WithDraw";
				break;
			case 2:
				title = "Deposit";
				break;
			default:
				title = "History";
				break;
		}
		this.props.changeTitleHome(title);
	}

	render() {
		let { activeTabIndex } = this.state;
		arr = [0, 1, 2, 3];
		console.log("render");
		return (
			<Tabs
				// // onChangeTab={this._handleOnChangeTab}
				// onChangeTab={tab => {
				// 	this.setState({
				// 		activeTabIndex: tab.i
				// 	});
				// }}
				// initialPage={0}
				// initialPage={1}
				locked
				onChangeTab={this._handleOnChangeTab}
				tabBarPosition="bottom"
				tabBarUnderlineStyle={{
					backgroundColor: "transparent"
				}}
			>
				{this._renderDashBoardTab()}
				{this._renderSendTab()}
				{this._renderRequestTab()}
				{this._renderExchangeTab()}
			</Tabs>
		);
	}

	_renderDashBoardTab = () => (
		<Tab
			heading={
				<TabHeading
					style={{
						flexDirection: "column",
						backgroundColor: "white"
					}}
				>
					<Icon
						size={
							this.state.activeTabIndex == 0
								? ICON_SIZE_ACTIVE
								: ICON_SIZE_UNACTIVE
						}
						name="apps"
						type="font-awesome"
						name="home"
						color={
							this.state.activeTabIndex == 0
								? COLOR_TAB_ACTIVE
								: COLOR_TAB_UNACTIVE
						}
					/>
				</TabHeading>
			}
		>
			<DashboardTab />
		</Tab>
	);
	_renderSendTab = () => (
		<Tab
			heading={
				<TabHeading
					style={{
						flexDirection: "column",
						backgroundColor: "white"
					}}
				>
					<Icon
						name="apps"
						type="material-community"
						name="send"
						size={
							this.state.activeTabIndex == 1
								? ICON_SIZE_ACTIVE
								: ICON_SIZE_UNACTIVE
						}
						color={
							this.state.activeTabIndex == 1
								? COLOR_TAB_ACTIVE
								: COLOR_TAB_UNACTIVE
						}
					/>
				</TabHeading>
			}
		>
			<SendTab />
		</Tab>
	);

	_renderExchangeTab = () => (
		<Tab
			heading={
				<TabHeading
					style={{
						flexDirection: "column",
						backgroundColor: "white"
					}}
				>
					<Icon
						name="apps"
						type="font-awesome"
						name="credit-card"
						size={
							this.state.activeTabIndex == 3
								? ICON_SIZE_ACTIVE
								: ICON_SIZE_UNACTIVE
						}
						color={
							this.state.activeTabIndex == 3
								? COLOR_TAB_ACTIVE
								: COLOR_TAB_UNACTIVE
						}
					/>
				</TabHeading>
			}
		>
			<ExchangeTab />
		</Tab>
	);

	_renderRequestTab = () => (
		<Tab
			heading={
				<TabHeading
					style={{
						flexDirection: "column",
						backgroundColor: "white"
					}}
				>
					<Icon
						name="apps"
						type="font-awesome"
						name="download"
						size={
							this.state.activeTabIndex == 2
								? ICON_SIZE_ACTIVE
								: ICON_SIZE_UNACTIVE
						}
						color={
							this.state.activeTabIndex == 2
								? COLOR_TAB_ACTIVE
								: COLOR_TAB_UNACTIVE
						}
					/>
				</TabHeading>
			}
		>
			<RequestTab />
		</Tab>
	);
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators(Action, dispatch);
}

export default connect(
	null,
	mapDispatchToProps
)(HomeBottomTab);
