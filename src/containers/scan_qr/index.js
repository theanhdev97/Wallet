import React, { Component } from "react";
import { View } from "react-native";
import Scan from "react-native-qrscanner-kit";
import QRCodeScanner from "react-native-qrcode-scanner";
import { Actions } from "react-native-router-flux";
import { random } from "../../utils/mathHelper";

// REDUX
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Action from "../../actions"; //Import your actions

class ScanQR extends Component {
	onSuccess(e) {
		Actions.pop();
		let result = e.data + random();
		this.props.scanQRCode(result);
	}

	render() {
		return <QRCodeScanner onRead={this.onSuccess.bind(this)} />;
	}
}

// function mapStateToProps(state, props) {
//     return {
//         loading: state.fetchDataReducer.loading,
//         data: state.fetchDataReducer.data
//     };
// }

function mapDispatchToProps(dispatch) {
	return bindActionCreators(Action, dispatch);
}

//Connect everything
export default connect(
	null,
	mapDispatchToProps
)(ScanQR);
