import { StyleSheet } from "react-native";

const style = StyleSheet.create({
	header: {
		// backgroundColor: "#FFD600"
		backgroundColor: "#3366CC"
	},
	headerTitle: {
		color: "#FFFDE7"
	},
	textItemTypeCoin: {
		textAlign: "center",
		color: "#21AEE9",
		fontSize: 18
	},
	buttonItemTypeCoin: {
		width: 200,
		justifyContent: "center"
	}
});

export default style;
