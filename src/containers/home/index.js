import React, { Component } from "react";
import { View ,StatusBar} from "react-native";
import {
	Container,
	Footer,
	FooterTab,
	Button,
	Content,
	Drawer,
	Header,
	Left,
	Body,
	Right,
	Title
} from "native-base";
import { Text } from "react-native";
import { Icon } from "react-native-elements";
import style from "./style";
import HomeBottomTab from "../../components/home_bottom_tab/index";
import DrawerMenu from "../../components/drawer_menu/index";
import { Actions } from "react-native-router-flux";
import PopupDialog, { DialogTitle } from "react-native-popup-dialog";

import { TYPE_COINS } from "../../utils/constants";

// REDUX
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Action from "../../actions"; //Import your actions

// import CustomAlert from "../../components/custom_alert/index";
// import Drawer from 'react-native-drawer'

var context = "";
var currentScreen = "";

import Data from "../../model/data";

import MTransaction from "../../model/MTransaction";

class Home extends Component {
	static _sShowTypeCoinDialog(screen) {
		currentScreen = screen;
		context.chooseTypeCoinDialog.show();
	}

	constructor(props) {
		super(props);
		context = this;
	}

	componentDidMount() {
		this.props.fetchCoinData();
	}

	_handleOnPressDrawerMenu = () => {
		this._openDrawer();
	};

	_handleOnPressScanQRMenu = () => {
		Actions.ScanQR();
	};

	_handleChooseCoinType = coinType => () => {
		this.chooseTypeCoinDialog.dismiss();
		this.props.changeCoinType(currentScreen, coinType);
	};

	_onPressDrawerMenuItem = menu => {
		this._closeDrawer();
	};

	_closeDrawer = () => {
		this.drawer._root.close();
		StatusBar.setHidden(false);
	};

	_openDrawer = () => {
		this.drawer._root.open();
		StatusBar.setHidden(true);
	};
	render() {
		return (
			<Drawer
				ref={ref => {
					this.drawer = ref;
				}}
				content={this._renderDrawerMenu()}
				onClose={this._closeDrawer}
			>
				{this._renderHeader()}
				<HomeBottomTab />
				{this._renderChooseCoinTypePopupDialog()}
			</Drawer>
		);
	}

	_renderDrawerMenu = () => (
		<DrawerMenu onPressDrawerMenuItem={this.onPressDrawerMenuItem} />
	);

	_renderHeader = () => (
		<Header style={style.header}>
			<Left>
				<Button
					style={{ marginLeft: 3 }}
					transparent
					onPress={this._handleOnPressDrawerMenu}
				>
					<Icon name="bars" type="font-awesome" color="#FFFDE7" />
				</Button>
			</Left>
			<Body>
				<Title style={style.headerTitle}>{this.props.title}</Title>
			</Body>
			<Right>
				<Button transparent onPress={this._handleOnPressScanQRMenu}>
					<Icon
						name="qrcode-scan"
						type="material-community"
						color="#FFFDE7"
					/>
				</Button>
			</Right>
		</Header>
	);

	_renderChooseCoinTypePopupDialog = () => (
		<PopupDialog
			width={200}
			height={420}
			dialogTitle={<DialogTitle title="Choose Coin" />}
			ref={dialog => {
				this.chooseTypeCoinDialog = dialog;
			}}
		>
			<View
				style={{
					flex: 1,
					justifyContent: "center"
				}}
			>
				{TYPE_COINS.map(item => (
					<Button
						key={item}
						transparent
						style={style.buttonItemTypeCoin}
						onPress={this._handleChooseCoinType(item)}
					>
						<Text style={style.textItemTypeCoin}>{item}</Text>
					</Button>
				))}
			</View>
		</PopupDialog>
	);
	// <View
	// 	style={{
	// 		backgroundColor: "gray",
	// 		marginLeft: 30,
	// 		marginRight: 30,
	// 		height: 0.3
	// 	}}
	// />
}

function mapStateToProps(state, props) {
	return {
		title: state.changeTitleHomeReducer.title
	};
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators(Action, dispatch);
}

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Home);
