import { StyleSheet } from "react-native";

const style = StyleSheet.create({
	home_left_menu: {
		marginLeft: 10
	},
	home_left_menu_icon: {
		color: "#FFFDE7"
	},
	home_right_menu_icon: {
		color: "#FFFDE7"
	},
	home_right_menu: {
		marginRight: 10
	},
	home_title: {
		textAlign: "left",
		paddingLeft: 10,
		width: 300,
		color: "#FFFDE7"
	},
	home_navigation: {
		backgroundColor: "#3366CC"
	}
});

export default style;
