import React, { Component } from "react";
import { Router, Stack, Scene, Actions } from "react-native-router-flux";
import { Icon } from "react-native-elements";
import { View } from "react-native";
import style from "./style";

import Splash from "../../components/splash/index";
import Home from "../home/index";
import QRCodeScanner from "react-native-qrcode-scanner";
import VerifyPin from "../../components/verify_pin/index";
import Login from "../login/index";
import Register from "../register/index";

import ScanQR from "../scan_qr/index";

console.disableYellowBox = true;

import FireBase from "../../api/firebase";

class App extends Component {
	constructor(props) {
		super(props);
	}

	_handleOnPressScanQRBackMenu = () => Actions.pop();

	render() {
		return (
			<Router>
				<Stack key="root">
					<Scene
						key="Login"
						component={Login}
						initial
						hideNavBar
						type="replace"
					/>
					<Scene
						key="VerifyPin"
						// initial
						component={VerifyPin}
						hideNavBar
					/>
					<Scene key="Splash" component={Splash} hideNavBar />
					<Scene
						// initial
						type="replace"
						key="Home"
						component={Home}
						title="Home"
						hideNavBar
					/>
					<Scene
						navigationBarStyle={style.home_navigation}
						key="ScanQR"
						component={ScanQR}
						title={"Scan QR"}
						titleStyle={style.home_title}
						renderLeftButton={this._renderScanQRLeftButton}
					/>
					<Scene hideNavBar key="Register" component={Register}   />
				</Stack>
			</Router>
		);
	}

	_renderScanQRLeftButton = () => (
		<View
			style={style.home_left_menu}
			hitSlop={{ top: 20, bottom: 20, left: 50, right: 50 }}
		>
			<Icon
				name="arrow-left"
				type="material-community"
				color="#FFFDE7"
				// onPress={this._handleOnPressScanQRBackMenu}
				onPress={this._handleOnPressScanQRBackMenu}
			/>
		</View>
	);
}

export default App;
