import React, { Component } from "react";
import {
	FlatList,
	Dimensions,
	Text,
	View,
	Image,
	ScrollView,
	RefreshControl
} from "react-native";
import {
	Card,
	Container,
	CardItem,
	Title,
	Body,
	Content,
	Left,
	Button,
	Right
} from "native-base";
import { Icon, Divider } from "react-native-elements";
import style from "./style";
import PieChartExample from "../../components/pie_chart/index";
import LoadingView from "../../components/loading_view/index";

// REDUX
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Action from "../../actions"; //Import your actions
TOTAL = "300,000,000";
import Data from "../../model/data";

let context;
import MCoin from "../../model/MCoin";
import FireBase from "../../api/firebase";

function insertStringAtIndex(position, string, token) {
	var output = string.substr(0, position) + token + string.substr(position);
	return output;
}

let COIN_INFO = [
	{
		coin: "NATO / USD",
		orientation: 1.61,
		rate: 1.105,
		volume: "253,143,378.74 USD"
	},
	{
		coin: "NATO / SGD",
		orientation: 1.82,
		rate: 1.12,
		volume: "411,299.79 SGD"
	},
	{
		coin: "BTC / NATO",
		orientation: 1.56,
		rate: "6,900.00",
		volume: "29,991,763.00 NATO"
	},
	{
		coin: "ETH / NATO",
		orientation: 7.86,
		rate: "712.00",
		volume: "15,991,008.00 NATO"
	},
	{
		coin: "XRP / NATO",
		orientation: 13.58,
		rate: 2.65,
		volume: "7,991,673.00 NATO"
	},

	{
		coin: "NATO / EUR",
		orientation: 3.52,
		rate: 0.93,
		volume: "7,991,763.00 NATO"
	},

	{
		coin: "NATO / CNY",
		orientation: 8.18,
		rate: 6.95,
		volume: "9,888,768.00 NATO"
	}
];

class DashboardTab extends Component {
	constructor(props) {
		super(props);
		context = this;
		this.state = {
			refreshing: false,
			listCoins: [],
			totalBalance: " "
		};

		// variable
		this.mListCoins = [];

		this._handleOnGetDataSuccess = this._handleOnGetDataSuccess.bind(this);
	}

	componentDidMount() {
		this._handleOnRefresh();
	}

	shouldComponentUpdate(nextProps, nextState) {
		if (
			// this.props.bitcoinBalance != nextProps.bitcoinBalance ||
			// this.props.bitcoinRate != nextProps.bitcoinRate ||
			// this.props.ethereumBalance != nextProps.ethereumBalance ||
			// this.props.ethereumRate != nextProps.ethereumRate ||
			// this.props.natoBalance != nextProps.natoBalance ||
			// this.props.natoRate != nextProps.natoRate ||
			this.state.refreshing != nextState.refreshing ||
			this.state.totalBalance != nextState.totalBalance
		)
			return true;
		return false;
	}

	componentWillReceiveProps(nextProps) {}

	_handleOnGetDataSuccess(listCoins) {
		console.log("HandleOnGetDataSuccess()");
		this.mListCoins = listCoins;
		let totalBalance = this._parseNumberToMoney(this._countTotalUSD());

		this.setState({
			refreshing: false,
			listCoins: this.mListCoins,
			totalBalance: totalBalance
		});
	}

	_updateData = () => {
		FireBase.getListCoins(context._handleOnGetDataSuccess);
	};

	_handleOnRefresh = () => {
		this.mListCoins = [];
		this.setState({
			refreshing: true
		});
		this._updateData();
	};

	_parseNumberToMoney(money) {
		let number = money.toFixed(0);
		let str = number.toString();

		let count = 0;
		// str = insertStringAtIndex (3,str,',');
		for (let i = str.length - 1; i >= 0; i--) {
			count++;
			if (count == 3 && i >= 1) {
				str = insertStringAtIndex(i, str, ",");
				count = 0;
			}
		}

		let totalBalance = "$ " + str;
		// this.props.changeTotalBalance(totalBalance);
		// this.setState({ totalBalance: totalBalance });

		return totalBalance;
	}

	render() {
		let { refreshing } = this.state;
		// if (refreshing) return <LoadingView />;
		return (
			// <Container style={style.container}>
			<ScrollView
				refreshControl={
					<RefreshControl
						refreshing={false}
						onRefresh={this._handleOnRefresh}
					/>
				}
			>
				{refreshing ? (
					<LoadingView />
				) : (
					<View style={style.container}>
						{this._renderTitleBalance()}
						<Card style={{ padding: 10 }}>
							{this._renderTotal()}
							{this._renderBalanceChart()}
							{this._renderBalanceHint()}
						</Card>
						{this._renderTitlePriceChart()}
						{this._renderListCardItem()}
					</View>
				)}
			</ScrollView>
			// </Container>
		);
	}

	_renderTotal = () => (
		<View
			// style={{
			// 	padding: 10,
			// 	marginBottom: 10,
			// 	alignItems: "center",
			// 	flexDirection: "row",
			// 	justifyContent: "flex-end"
			// }}
			style={{
				padding: 10,
				marginBottom: 10,
				alignItems: "center",
				justifyContent: "flex-end"
			}}
		>
			<Text style={{ color: "black", fontSize: 15, fontWeight: "bold" }}>
				Portfolio Balance
			</Text>
			<Text
				style={{
					color: "#3366CC",
					fontSize: 20,
					fontWeight: "bold"
				}}
			>
				{this.state.totalBalance}
			</Text>
		</View>
	);

	_renderBalanceHint = () => (
		// {BALANCE.map((item, index) =>
		// 	this._renderItemBalanceHint(item, index)
		// )}
		<View>
			<View
				style={{
					flex: 4,
					flexWrap: "wrap",
					flexDirection: "row",
					paddingTop: 20,
					paddingBottom: 20,
					justifyContent: "space-between"
				}}
			>
				{this._renderFirstRowBalance()}
			</View>
			<View
				style={{
					flex: 4,
					flexWrap: "wrap",
					flexDirection: "row",
					paddingTop: 0,
					paddingBottom: 20,
					justifyContent: "space-between"
				}}
			>
				{this._renderSecondRowBalance()}
			</View>
		</View>
	);

	_renderFirstRowBalance() {
		let { listCoins } = this.state;
		if (listCoins.length > 0) {
			let arr = [];
			for (let i = 0; i < 4; i++)
				arr.push(this._renderItemBalanceHint(listCoins[i], i));
			return arr;
		}
		return null;
	}

	_renderSecondRowBalance() {
		let { listCoins } = this.state;
		if (listCoins.length > 0) {
			let arr = [];
			for (let i = 4; i < 8; i++)
				arr.push(this._renderItemBalanceHint(listCoins[i], i));
			return arr;
		}
		return null;
	}

	_renderItemBalanceHint = (item, index) => (
		<View style={{ flex: 1, alignItems: "center" }} key={item.name}>
			{this._renderDivider(item.color)}
			<Text style={style.balanc_hint_item_name}>{item.name}</Text>
			<Text style={style.balanc_hint_item_balance}>
				{item.balance} {item.type}
			</Text>
		</View>
	);

	_renderDivider = color => (
		<View
			style={[
				{
					backgroundColor: color
				},
				style.divider
			]}
		/>
	);

	_renderTitleBalance = () => <Title style={style.title}>Balance</Title>;

	// _renderBalanceChart = () => (
	// 	<PieChartExample colors={CHART_COLORS} percents={CHART_PERCENTS} />
	// );

	_countTotalUSD = () => {
		let total = 0;
		this.mListCoins.map(item => {
			total += item.balance * item.rate;
		});
		console.log("Percent : " + total);
		return total;
	};

	_renderBalanceChart = () => {
		let CHART_COLOR = [];
		let CHART_PERCENT = [];
		let total = this._countTotalUSD();

		console.log("SIZE CHART : " + this.mListCoins.length);

		this.mListCoins.map(item => {
			CHART_COLOR.push(item.color);
			let percent = ((item.balance * item.rate) / total) * 100;
			console.log("Percent : " + percent);
			CHART_PERCENT.push(percent);
		});
		return (
			<PieChartExample colors={CHART_COLOR} percents={CHART_PERCENT} />
		);
	};

	_renderTitlePriceChart = () => (
		<Title style={[style.title, { marginTop: 10 }]}>Price chart</Title>
	);

	_renderPriceChart = () => (
		<Content style={{ margin: 10 }}>{this._renderListCardItem()}</Content>
	);

	// _renderListCardItem() {
	// 	let arr = [];
	// 	// PRICE_CHART.map((item, index) => {
	// 	// 	arr.push(this._renderCardItem(item, index));
	// 	// });
	// 	let { listCoins } = this.state;
	// 	listCoins.map((item, index) => {
	// 		arr.push(this._renderCardItem(item, index));
	// 	});
	// 	return arr;
	// }
	_renderListCardItem() {
		let arr = [];
		// PRICE_CHART.map((item, index) => {
		// 	arr.push(this._renderCardItem(item, index));
		// });
		// let { listCoins } = this.state;
		COIN_INFO.map((item, index) => {
			arr.push(this._renderCardItem(item, index));
		});
		return arr;
	}

	// _renderCardItem = (item, index) => (
	// 	<Card style={{ backgroundColor: "white" }} key={item.name}>
	// 		<CardItem>
	// 			<View style={{ alignItems: "flex-start" }}>
	// 				<Title style={style.price_chart_item_title}>
	// 					{item.name} / USD
	// 				</Title>
	// 				<Title style={style.price_chart_item_price}>
	// 					{item.rate.toFixed(3)}
	// 				</Title>
	// 				<Title style={style.price_chart_item_volume}>
	// 					Volume : {item.volume} USD
	// 				</Title>
	// 			</View>
	// 			<Title style={style.price_chart_item_orientation}>
	// 				{item.orientation} %
	// 			</Title>
	// 		</CardItem>
	// 	</Card>
	// );

	_renderCardItem = (item, index) => (
		<Card style={{ backgroundColor: "white" }} key={item.name}>
			<CardItem>
				<View style={{ alignItems: "flex-start" }}>
					<Title style={style.price_chart_item_title}>
						{item.coin}
					</Title>
					<Title style={style.price_chart_item_price}>
						{item.rate}
					</Title>
					<Title style={style.price_chart_item_volume}>
						Volume : {item.volume}
					</Title>
				</View>
				<Title style={style.price_chart_item_orientation}>
					{item.orientation} %
				</Title>
			</CardItem>
		</Card>
	);
}

// function mapStateToProps(state, props) {
// 	return {
// 		totalBalance: state.coinReducer.totalBalance,

// 		// // balance
// 		// bitcoinBalance: state.coinReducer.bitcoinBalance,
// 		// ethereumBalance: state.coinReducer.ethereumBalance,
// 		// natoBalance: state.coinReducer.natoBalance,
// 		// // rate
// 		// bitcoinRate: state.coinReducer.bitcoinRate,
// 		// ethereumRate: state.coinReducer.ethereumRate,
// 		// natoRate: state.coinReducer.natoRate
// 	};
// }

function mapDispatchToProps(dispatch) {
	return bindActionCreators(Action, dispatch);
}

export default connect(
	null,
	mapDispatchToProps
)(DashboardTab);
