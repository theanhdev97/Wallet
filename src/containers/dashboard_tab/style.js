import { StyleSheet, Dimensions } from "react-native";

const style = StyleSheet.create({
	container: {
		backgroundColor: "white",
		padding: 10
	},
	title: {
		textAlign: "left",
		width: Dimensions.get("window").width - 20,
		fontSize: 20,
		color: "black",
		marginLeft: 3,
		paddingBottom: 10
	},
	price_chart_item_title: {
		color: "black",
		fontSize: 21,
		paddingBottom: 4
	},
	price_chart_item_price: {
		color: "#CC0066",
		fontSize: 18,
		paddingBottom: 4
	},
	price_chart_item_volume: {
		color: "gray",
		fontSize: 16,
		paddingBottom: 4
	},
	price_chart_item_orientation: {
		position: "absolute",
		right: 10,
		top: 10,
		color: "#6D8B1D"
	},
	balanc_hint_item_name: {
		color: "gray",
		fontSize: 13,
		fontWeight: "bold"
	},
	balanc_hint_item_balance: {
		color: "black",
		fontSize: 13
	},
	coin_image: {
		width: 50,
		height: 20
	},
	divider: {
		height: 2,
		width: 50,
		margin: 10
	}
});

export default style;
