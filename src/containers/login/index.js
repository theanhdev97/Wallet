import React, { Component } from "react";
import {
	FlatList,
	Dimensions,
	View,
	Alert,
	Image,
	ScrollView,
	RefreshControl,
	KeyboardAvoidingView,
	Keyboard,
	Platform,
	TouchableOpacity
} from "react-native";
import {
	Text,
	Container,
	Title,
	Body,
	Content,
	Button,
	List,
	ListItem,
	Left,
	Right,
	ActionSheet,
	Accordion,
	Form,
	Label,
	Input,
	Item,
	Icon as NBIcon
} from "native-base";
import { Icon } from "react-native-elements";
import style from "./style";
import Home from "../home/index";
import LoadingView from "../../components/loading_view/index";
import MTransaction from "../../model/MTransaction";
import {
	BallIndicator,
	BarIndicator,
	DotIndicator,
	MaterialIndicator,
	PacmanIndicator,
	PulseIndicator,
	SkypeIndicator,
	UIActivityIndicator,
	WaveIndicator
} from "react-native-indicators";
import { WanderingCubes } from "react-native-spinkit";
// var Spinkit = require("react-native-spinkit");
import Spinner from "react-native-loading-spinner-overlay";
var Spinkit = require("react-native-spinkit");
import CustomAlert from "../../components/custom_alert/index";
import { Actions } from "react-native-router-flux";

// REDUX
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Action from "../../actions"; //Import your actions

import Data from "../../model/data";
import { getCurrentDate } from "../../utils/dateHelper";
import FireBase from "../../api/firebase";
import VerifyPin from "../../components/verify_pin/index";

// let FORUM = " Forum  / ";
let FORUM = " Forum";
// let HELP = " Help  / ";
let HELP = " Help";
// let ENGLISH = " English ";
let ENGLISH = " English";

let IC_LOGO = require("../../../assets/image/ic_logo_nato_exchange.png");

var context;

export default class Login extends Component {
	static loginWithAccount1() {
		context.userName = "ricky@gmail.com";
		context.userPassword = "ricky123";
		context._handleOnSignIn();
		// setTimeout(() => {
		// 	VerifyPin.verifyPinForAccTest();
		// }, 2500);
	}

	static loginWithAccount2() {
		context.userName = "lisa@gmail.com";
		context.userPassword = "lisa123";
		context._handleOnSignIn();
		// setTimeout(() => {
		// 	VerifyPin.verifyPinForAccTest();
		// }, 2500);
	}

	constructor(props) {
		super(props);
		context = this;
		this.state = {
			submitting: false
		};
		this.userName = "";
		this.userPassword = "";

		this.mHeight = Platform.select({ ios: 7, android: 7 });
		this.mHeaderHeight = Platform.select({ ios: 2, android: 2 });
		this.mBodyHeight = Platform.select({ ios: 4, android: 4 });
		this.mFooterHeight = Platform.select({ ios: 1, android: 1 });
	}

	shouldComponentUpdate(props, state) {
		if (this.state.submitting != state.submitting) return true;
		return false;
	}

	_handleOnPressForgotYourPassword = () => {
		alert("this feature is comming soon");
	};

	_handleOnChangeUserName = input => {
		this.userName = input;
	};

	_handleOnChangeUserPassword = input => {
		this.userPassword = input;
	};

	_handleOnPressHelp = () => {
		alert("this feature is comming soon");
	};

	_handleOnPressForum = () => {
		alert("this feature is comming soon");
	};

	_handleOnPressEnglish = () => {
		alert("this feature is comming soon");
	};

	_handleOnPressRegister = () => {
		Actions.Register();
	};

	_handleOnSignIn = () => {
		this.setState({
			submitting: true
		});
		setTimeout(() => {
			this.setState({
				submitting: false
			});

			setTimeout(() => {
				if (FireBase.loginAccount(this.userName, this.userPassword))
					Actions.VerifyPin();
				else alert("Incorrect user name or password");
			}, 200);
		}, 2000);
	};

	render() {
		return (
			<KeyboardAvoidingView
				style={{
					backgroundColor: "white",
					flex: this.mHeight
				}}
				behavior="padding"
				enabled={Platform.select({ ios: true, android: false })}
			>
				<View
					style={{
						flex: this.mHeaderHeight,
						// justifyContent: "center",
						alignItems: "center",
						justifyContent: "flex-end"
					}}
				>
					<Image
						resizeMode="stretch"
						source={IC_LOGO}
						style={{ width: 200, height: 100 }}
					/>
				</View>
				{this._renderForm()}
				{this._renderFooter()}
				{this._renderSendingOverlay()}
			</KeyboardAvoidingView>
		);
	}

	_renderForm = () => (
		// <View style={{ flex: this.mBodyHeight }}>
		<View style={{ flex: this.mBodyHeight, justifyContent: "center" }}>
			<Form style={{ margin: 20, marginRight: 35, alignItems: "center" }}>
				<Item style={{ marginBottom: 10 }}>
					<Input
						style={{ fontSize: 18 }}
						placeholderTextColor="black"
						blurOnSubmit={true}
						placeholder={`Email address`}
						placeholderTextColor="gray"
						onChangeText={this._handleOnChangeUserName}
					/>
				</Item>
				<Item>
					<Input
						style={{ fontSize: 18 }}
						placeholderTextColor="black"
						blurOnSubmit={true}
						placeholder={`Password`}
						secureTextEntry={true}
						placeholderTextColor="gray"
						onChangeText={this._handleOnChangeUserPassword}
					/>
				</Item>
			</Form>

			<View style={{ marginLeft: 20, marginRight: 20 }}>
				<Button full warning onPress={this._handleOnSignIn}>
					<Text style={{ color: "#FFF3E0" }}>Sign in</Text>
				</Button>
			</View>

			<Button
				transparent
				onPress={this._handleOnPressForgotYourPassword}
				style={{
					marginTop: 5,
					width: "100%",
					// backgroundColor: 'pink',
					// width: 150,
					marginLeft: 0,
					paddingLeft: 0,
					justifyContent: "center"
				}}
			>
				<Text
					style={{
						textAlign: "center",
						color: "orange"
					}}
				>
					Forgot your password ?
				</Text>
			</Button>

			<View
				style={{
					flexDirection: "row",
					paddingLeft: 20,
					justifyContent: "center",
					// paddingRight: -50,
					// backgroundColor: "pink"
					// marginRight: 20
					marginBottom: 10
				}}
			>
				<Text style={{ color: "black" }}>Don't have account yet ?</Text>
				<TouchableOpacity onPress={this._handleOnPressRegister}>
					<Text style={{ color: "orange", marginLeft: 10 }}>
						Register
					</Text>
				</TouchableOpacity>
			</View>
		</View>
		// <Button full>
	);

	_renderFooter = () => (
		<View
			style={{
				// marginBottom: 10,
				marginLeft: 80,
				marginRight: 80,
				marginBottom: 20,
				flex: this.mFooterHeight,
				justifyContent: "space-between",
				alignItems: "flex-end",
				flexDirection: "row"
			}}
		>
			<TouchableOpacity transparent onPress={this._handleOnPressForum}>
				<Text
					style={{
						color: "gray",
						textAlign: "center",
						marginBottom: 5,
						fontWeight: "bold"
					}}
				>
					{FORUM}
				</Text>
			</TouchableOpacity>
			<TouchableOpacity transparent onPress={this._handleOnPressHelp}>
				<Text
					style={{
						color: "gray",
						textAlign: "center",
						marginBottom: 5,
						fontWeight: "bold"
					}}
				>
					{HELP}
				</Text>
			</TouchableOpacity>
			<TouchableOpacity
				style={{ flexDirection: "row" }}
				onPress={this._handleOnPressEnglish}
			>
				<Text
					style={{
						color: "gray",
						textAlign: "center",
						marginRight: 10,
						fontWeight: "bold"
					}}
				>
					{ENGLISH}
				</Text>
				<Icon name="language" type="font-awesom" color="gray" />
			</TouchableOpacity>
		</View>
	);

	_renderSendingOverlay = () => (
		<Spinner visible={this.state.submitting} cancelable={false}>
			<View
				style={{
					flex: 1,
					justifyContent: "center",
					alignItems: "center"
				}}
			>
				<View
					style={{
						backgroundColor: "white",
						justifyContent: "center",
						alignItems: "center",
						width: 300,
						height: 200,
						borderRadius: 5,
						borderWidth: 1,
						borderColor: "black"
					}}
				>
					<Spinkit
						isVisible={true}
						color="orange"
						size={60}
						type={"9CubeGrid"}
					/>
					<Text
						style={{
							marginTop: 10,
							fontSize: 30,
							color: "orange"
						}}
					>
						Sign In
					</Text>
				</View>
			</View>
		</Spinner>
	);
}
