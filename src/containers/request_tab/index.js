import React, { Component } from "react";
import { FlatList, Dimensions, View, Alert, Image } from "react-native";
import {
	Text,
	Container,
	Title,
	Body,
	Content,
	Button,
	List,
	ListItem,
	Left,
	Right,
	ActionSheet,
	Accordion
} from "native-base";
import { Icon } from "react-native-elements";
import style from "./style";
import Home from "../home/index";
import FireBase from "../../api/firebase";

// REDUX
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Action from "../../actions"; //Import your actions

class RequestTab extends Component {
	constructor(props) {
		super(props);
	}

	shouldComponentUpdate(nextProps, nextState) {
		if (this.props.typeCoin != nextProps.typeCoin) return true;
		return false;
	}

	_showTypeCoinDialog = () => {
		Home._sShowTypeCoinDialog("request");
	};

	render() {
		let { typeCoin } = this.props;
		return (
			<Container>
				<Content style={{ backgroundColor: "white" }}>
					{this._renderHeader(typeCoin)}
					{this._renderBody()}
				</Content>
			</Container>
		);
	}

	_renderHeader = typeCoin => (
		<View
			style={{
				backgroundColor: "#3366CC",
				padding: 10,
				justifyContent: "center",
				alignItems: "center"
			}}
		>
			<View
				style={{
					// borderStyle:'pink',
					flexDirection: "row",
					alignItems: "center",
					justifyContent: "center",
					marginTop: 5
				}}
			>
				<Button transparent onPress={this._showTypeCoinDialog}>
					{typeCoin == "BTC" && (
						<Icon
							name="bitcoin-circle"
							type="foundation"
							size={30}
							// color={"#FFFDE7"}
							color={"#FFD600"}
						/>
					)}

					{typeCoin == "ETH" && (
						<Icon
							name="ethereum"
							type="material-community"
							size={30}
							// color={"#FFFDE7"}
							color={"#FFD600"}
						/>
					)}

					{typeCoin == "NATO" && (
						<Image
							resizeMode="stretch"
							source={require("../../../assets/image/ic_logo.png")}
							style={{ width: 30, height: 30 }}
						/>
					)}

					{typeCoin == "USD" && (
						<Image
							resizeMode="stretch"
							source={require("../../../assets/image/ic_usd.png")}
							style={{ width: 30, height: 30 }}
						/>
					)}
					{typeCoin == "EUR" && (
						<Image
							resizeMode="stretch"
							source={require("../../../assets/image/ic_euro.png")}
							style={{ width: 30, height: 30 }}
						/>
					)}
					{typeCoin == "XRP" && (
						<Image
							resizeMode="stretch"
							source={require("../../../assets/image/ic_xrp.png")}
							style={{ width: 30, height: 30 }}
						/>
					)}
					{typeCoin == "CNY" && (
						<Image
							resizeMode="stretch"
							source={require("../../../assets/image/ic_cny.png")}
							style={{ width: 30, height: 30 }}
						/>
					)}
					{typeCoin == "SGD" && (
						<Image
							resizeMode="stretch"
							source={require("../../../assets/image/ic_sgd.png")}
							style={{ width: 30, height: 30 }}
						/>
					)}

					<Text
						style={{
							// color: "#FFFDE7",
							color: "#FFD600",
							marginLeft: 5,
							marginRight: 5,
							fontWeight: "bold",
							fontSize: 20
						}}
					>
						{typeCoin}
					</Text>
					<Icon
						name="angle-down"
						type="font-awesome"
						size={20}
						// color={"#FFFDE7"}
						color={"#FFD600"}
					/>
				</Button>
			</View>
		</View>
	);

	_renderBody = () => (
		<View
			style={{
				flex: 1,
				backgroundColor: "white",
				justifyContent: "center",
				alignItems: "center"
			}}
		>
			<Text
				style={{
					color: "black",
					margin: 20,
					fontSize: 18,
					fontWeight: "bold"
				}}
			>
				Scan this image to get your address
			</Text>
			{this._renderImage()}

			{this._renderAddress()}
		</View>
	);

	_renderAddress() {
		let address = "";
		if (FireBase.INDEX == 0) {
			switch (this.props.typeCoin) {
				case "NATO":
					address = "0x9c44C47d78090EfFa77bAC56CC2a8e13Fe4236IF";
					break;
				case "BTC":
					address = "3Qz9HUx2MphbRjQoOG75X1GAH41fV5vFX5";
					break;
				case "ETH":
					address = "0x6c44C47d78090MfKm77bAC56CC2a8e13F77KN87";
					break;
				case "XRP":
					address = "rEb8TK3gKgk5auUkwc6sHnwrGVJH8DuaLh";
					break;
				default:
					address = `Your ${this.props.typeCoin} Wallet`;
			}
		}
		if (FireBase.INDEX == 1) {
			switch (this.props.typeCoin) {
				case "NATO":
					address = "0x8c55C47d78090EfFa82bAC56CC2a8e13Fe7690IF";
					break;
				case "BTC":
					address = "3Pz9HUx2MphbTjQoOG95X1JAH41fV6YUz5";
					break;
				case "ETH":
					address = "0x6c44C87d78009MfKm77bAC19CC2a8e13F77KN87";
					break;
				case "XRP":
					address = "Kbb8TK3gKgk5auUkwc6sHnwrGVJH8Jnmyt";
					break;

				default:
					address = `Your ${this.props.typeCoin} Wallet`;
					break;
			}
		}
		if (FireBase.INDEX == 2) {
			switch (this.props.typeCoin) {
				case "NATO":
					address = "0x8c65D47d78090EfFa82bAC56CD2a8e13Fe6621YU";
					break;

				case "BTC":
					address = "3Pz9HUx9NphbTjQoKG95X1JLR41fV6KMz9";
					break;
				case "ETH":
					address = "0x6c44C87d78449MfKm77bAC19CC2a8e13F77mM81";
					break;
				case "XRP":
					address = "Kbb8TK3gKgk5muUkwR6sHnwrGVJH8Jn90l";
					break;

				default:
					address = `Your ${this.props.typeCoin} Wallet`;
					break;
			}
		}
		if (FireBase.INDEX == 3) {
			switch (this.props.typeCoin) {
				case "NATO":
					address = "3Pz9HUx9NphbTjQoKG95X1JLR41fV6KML0";
					break;
				case "BTC":
					address = "3Pz9HUx2MphbTjQoOG95X1JAH41fV6Y98a";
					break;
				case "ETH":
					address = "0x6c44C87d78449MfKm77bAC19CC2a8e13F77m90a";
					break;
				case "XRP":
					address = "Kbb8TK3gKgk5muUkwR6sHnwrGVJH8Jn012";
					break;

				default:
					address = `Your ${this.props.typeCoin} Wallet`;
					break;
			}
		}
		if (FireBase.INDEX == 4) {
			switch (this.props.typeCoin) {
				case "NATO":
					address = "3Pz9HUx9NphbTjQoKG95X1JLR41fV6K012";
					break;

				case "BTC":
					address = "3Pz9HUx2MphbTjQoOG95X1JAH41fV6Y839";
					break;
				case "ETH":
					address = "0x6c44C87d78449MfKm77bAC19CC2a8e13F77m9as";
					break;
				case "XRP":
					address = "Kbb8TK3gKgk5muUkwR6sHnwrGVJH8Jn99l";
					break;
				default:
					address = `Your ${this.props.typeCoin} Wallet`;
					break;
			}
		}
		return (
			<Text
				style={{
					color: "gray",
					// margin: 10,
					textAlign: "center",
					padding: 20,
					fontSize: 13
				}}
			>
				{address}
			</Text>
		);
	}

	_renderImage() {
		if (FireBase.INDEX == 0) {
			switch (this.props.typeCoin) {
				case "BTC":
					return (
						<Image
							source={require(`../../../assets/image/acc_1_btc.png`)}
							style={{
								width: 200,
								height: 270,
								resizeMode: "cover"
							}}
						/>
					);
				case "ETH":
					return (
						<Image
							source={require(`../../../assets/image/acc_1_eth.png`)}
							style={{
								width: 200,
								height: 270,
								resizeMode: "cover"
							}}
						/>
					);
					break;
				case "NATO":
					return (
						<Image
							source={require(`../../../assets/image/acc_1_nato.png`)}
							style={{
								width: 200,
								height: 270,
								resizeMode: "cover"
							}}
						/>
					);
					{
						typeCoin == "USD" && (
							<Image
								resizeMode="stretch"
								source={require("../../../assets/image/ic_usd.png")}
								style={{ width: 30, height: 30 }}
							/>
						);
					}
					{
						typeCoin == "EUR" && (
							<Image
								resizeMode="stretch"
								source={require("../../../assets/image/ic_euro.png")}
								style={{ width: 30, height: 30 }}
							/>
						);
					}
					{
						typeCoin == "XRP" && (
							<Image
								resizeMode="stretch"
								source={require("../../../assets/image/ic_xrp.png")}
								style={{ width: 30, height: 30 }}
							/>
						);
					}
					{
						typeCoin == "CNY" && (
							<Image
								resizeMode="stretch"
								source={require("../../../assets/image/ic_cny.png")}
								style={{ width: 30, height: 30 }}
							/>
						);
					}
					{
						typeCoin == "SGD" && (
							<Image
								resizeMode="stretch"
								source={require("../../../assets/image/ic_sgd.png")}
								style={{ width: 30, height: 30 }}
							/>
						);
					}

				default:
					return (
						<Image
							source={require(`../../../assets/image/acc_1.png`)}
							style={{
								width: 200,
								height: 270,
								resizeMode: "cover"
							}}
						/>
					);
			}
		}

		if (FireBase.INDEX == 1) {
			switch (this.props.typeCoin) {
				case "BTC":
					return (
						<Image
							source={require(`../../../assets/image/acc_2_btc.png`)}
							style={{
								width: 200,
								height: 270,
								resizeMode: "cover"
							}}
						/>
					);
				case "ETH":
					return (
						<Image
							source={require(`../../../assets/image/acc_2_eth.png`)}
							style={{
								width: 200,
								height: 270,
								resizeMode: "cover"
							}}
						/>
					);
					break;
				case "XRP":
					return (
						<Image
							source={require(`../../../assets/image/acc_2_xrp.png`)}
							style={{
								width: 200,
								height: 270,
								resizeMode: "cover"
							}}
						/>
					);
				case "NATO":
					return (
						<Image
							source={require(`../../../assets/image/acc_2_nato.png`)}
							style={{
								width: 200,
								height: 270,
								resizeMode: "cover"
							}}
						/>
					);
				default:
					return (
						<Image
							source={require(`../../../assets/image/acc_2.png`)}
							style={{
								width: 200,
								height: 270,
								resizeMode: "cover"
							}}
						/>
					);
			}
		}

		if (FireBase.INDEX == 2) {
			switch (this.props.typeCoin) {
				case "BTC":
					return (
						<Image
							source={require(`../../../assets/image/acc_3_btc.png`)}
							style={{
								width: 200,
								height: 270,
								resizeMode: "cover"
							}}
						/>
					);
				case "ETH":
					return (
						<Image
							source={require(`../../../assets/image/acc_3_eth.png`)}
							style={{
								width: 200,
								height: 270,
								resizeMode: "cover"
							}}
						/>
					);
					break;
				case "XRP":
					return (
						<Image
							source={require(`../../../assets/image/acc_3_xrp.png`)}
							style={{
								width: 200,
								height: 270,
								resizeMode: "cover"
							}}
						/>
					);
				case "NATO":
					return (
						<Image
							source={require(`../../../assets/image/acc_3_nato.png`)}
							style={{
								width: 200,
								height: 270,
								resizeMode: "cover"
							}}
						/>
					);
				default:
					return (
						<Image
							source={require(`../../../assets/image/acc_1.png`)}
							style={{
								width: 200,
								height: 270,
								resizeMode: "cover"
							}}
						/>
					);
			}
		}

		if (FireBase.INDEX == 3) {
			switch (this.props.typeCoin) {
				case "BTC":
					return (
						<Image
							source={require(`../../../assets/image/acc_4_btc.png`)}
							style={{
								width: 200,
								height: 270,
								resizeMode: "cover"
							}}
						/>
					);
				case "ETH":
					return (
						<Image
							source={require(`../../../assets/image/acc_4_eth.png`)}
							style={{
								width: 200,
								height: 270,
								resizeMode: "cover"
							}}
						/>
					);
					break;
				case "XRP":
					return (
						<Image
							source={require(`../../../assets/image/acc_4_xrp.png`)}
							style={{
								width: 200,
								height: 270,
								resizeMode: "cover"
							}}
						/>
					);
				case "NATO":
					return (
						<Image
							source={require(`../../../assets/image/acc_4_nato.png`)}
							style={{
								width: 200,
								height: 270,
								resizeMode: "cover"
							}}
						/>
					);
				default:
					return (
						<Image
							source={require(`../../../assets/image/acc_4.png`)}
							style={{
								width: 200,
								height: 270,
								resizeMode: "cover"
							}}
						/>
					);
			}
		}

		if (FireBase.INDEX == 4) {
			switch (this.props.typeCoin) {
				case "BTC":
					return (
						<Image
							source={require(`../../../assets/image/acc_5_btc.png`)}
							style={{
								width: 200,
								height: 270,
								resizeMode: "cover"
							}}
						/>
					);
				case "ETH":
					return (
						<Image
							source={require(`../../../assets/image/acc_5_eth.png`)}
							style={{
								width: 200,
								height: 270,
								resizeMode: "cover"
							}}
						/>
					);
					break;
				case "XRP":
					return (
						<Image
							source={require(`../../../assets/image/acc_5_xrp.png`)}
							style={{
								width: 200,
								height: 270,
								resizeMode: "cover"
							}}
						/>
					);
				case "NATO":
					return (
						<Image
							source={require(`../../../assets/image/acc_5_nato.png`)}
							style={{
								width: 200,
								height: 270,
								resizeMode: "cover"
							}}
						/>
					);
				default:
					return (
						<Image
							source={require(`../../../assets/image/acc_5.png`)}
							style={{
								width: 200,
								height: 270,
								resizeMode: "cover"
							}}
						/>
					);
			}
		}
	}
}

function mapStateToProps(state, props) {
	return {
		typeCoin: state.changeTypeCoinReducer.requestTab
	};
}

export default connect(
	mapStateToProps,
	null
)(RequestTab);
