import React, { Component } from "react";
import { Container, Content, Body, Text, Button } from "native-base";
import { View, Image } from "react-native";
import { Actions } from "react-native-router-flux";
import Login from "../login/index";

let IC_LOGO = require("../../../assets/image/ic_logo_nato_exchange.png");

class Register extends Component {
	_handleOnPressLoginAccount1 = () => {
		Actions.pop();
		Login.loginWithAccount1();
	};

	_handleOnPressLoginAccount2 = () => {
		Actions.pop();
		Login.loginWithAccount2();
	};

	_handleOnPressBackToLogin = () => {
		Actions.pop();
	};

	render() {
		return (
			<Container
				style={{
					flex: 2,
					backgroundColor: "white",
					justifyContent: "center",
					alignItems: "center"
				}}
			>
				<Image
					resizeMode="stretch"
					source={IC_LOGO}
					style={{ width: 200, height: 100 }}
				/>
				<Text
					style={{
						marginTop: 30,
						fontSize: 16,
						color: "gray",
						textAlign: "center",
						padding: 20
					}}
				>
					This Version does not support register !!! Here is 2 test's
					account if you want to use
				</Text>
				<View style={{ marginLeft: 20, marginRight: 20 }}>
					<Button
						full
						warning
						onPress={this._handleOnPressLoginAccount1}
						style={{ marginTop: 30 }}
					>
						<Text style={{ color: "#FFF3E0" }}>
							Login with account 1
						</Text>
					</Button>
					<Text style={{ textAlign: "center" ,marginTop:10}}> Pin : 2940 </Text>
				</View>

				<View style={{ marginLeft: 20, marginRight: 20 }}>
					<Button
						full
						warning
						onPress={this._handleOnPressLoginAccount2}
						style={{ marginTop: 20 }}
					>
						<Text style={{ color: "#FFF3E0" }}>
							Login with account 2
						</Text>
					</Button>
					<Text style={{ textAlign: "center" ,marginTop:10}}> Pin : 3586 </Text>

				</View>
				<View style={{ marginLeft: 20, marginRight: 20 }}>
					<Button
						warning
						full
						onPress={this._handleOnPressBackToLogin}
						style={{ marginTop: 20 }}
					>
						<Text style={{ color: "#FFF3E0" }}>Back to login</Text>
					</Button>

				</View>
			</Container>
		);
	}
}

export default Register;
