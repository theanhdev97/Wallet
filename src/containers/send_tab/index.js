import React, { Component } from "react";
import {
	FlatList,
	Dimensions,
	View,
	Alert,
	Image,
	ScrollView,
	RefreshControl,
	KeyboardAvoidingView,
	Keyboard,
	Platform
} from "react-native";
import {
	Text,
	Container,
	Title,
	Body,
	Content,
	Button,
	List,
	ListItem,
	Left,
	Right,
	ActionSheet,
	Accordion,
	Form,
	Label,
	Input,
	Item,
	Icon as NBIcon
} from "native-base";
import { Icon } from "react-native-elements";
import style from "./style";
import Home from "../home/index";
import LoadingView from "../../components/loading_view/index";
import Spinner from "react-native-loading-spinner-overlay";
import MTransaction from "../../model/MTransaction";
import {
	BallIndicator,
	BarIndicator,
	DotIndicator,
	MaterialIndicator,
	PacmanIndicator,
	PulseIndicator,
	SkypeIndicator,
	UIActivityIndicator,
	WaveIndicator
} from "react-native-indicators";
import { WanderingCubes } from "react-native-spinkit";
var Spinkit = require("react-native-spinkit");
import CustomAlert from "../../components/custom_alert/index";

// REDUX
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Action from "../../actions"; //Import your actions

import Data from "../../model/data";
import { getCurrentDate } from "../../utils/dateHelper";
import FireBase from "../../api/firebase";

let SUCCESS = 0;
let BIGGER_THAN_BALANCE = 1;
let INVALID_NUMBER = 2;

var context;

class SendTab extends Component {
	static defaultProps = {
		toAddress: "",
		typeCoin: "NATO"
	};

	constructor(props) {
		super(props);
		context = this;
		this.state = {
			send: 0,
			fee: 0,
			feeUSD: 0,
			currentBalance: 0,
			currentFee: 0,
			currentRate: 0,
			currentCoinType: "NATO",
			sendMoneyInputState: SUCCESS,
			isValidTransaction: false,
			address: "",
			forceUpdate: false,
			refreshing: false,
			sending: false,
			isShowNotifySuccess: false
		};
		this.mSend = 0;
		this.mSendAddress = "";
		this.mSendMoneyInputState = SUCCESS;
		this.currentCoin = 0;
		this.nextTypeCoin = "NATO";

		// FireBase.sendMoneyFromBalance(10, "BTC", () => {});
		// FireBase.inputMoneyFromBalance(10, "BTC", () => {});
	}

	componentDidMount() {
		this.setState({
			sendMoneyInputState: SUCCESS
		});
		this._handleOnRefresh();
	}

	shouldComponentUpdate(nextProps, nextState) {
		if (
			this.props.toAddress != nextProps.toAddress ||
			this.props.typeCoin != nextProps.typeCoin ||
			this.state.send != nextState.send ||
			this.state.sendMoneyInputState != nextState.sendMoneyInputState ||
			this.state.currentBalance != nextState.currentBalance ||
			this.state.isValidTransaction != nextState.isValidTransaction ||
			this.state.address != nextState.address ||
			this.state.forceUpdate != nextState.forceUpdate ||
			this.state.refreshing != nextState.refreshing ||
			this.state.currentCoinType != nextState.currentCoinType ||
			this.state.sending != nextState.sending ||
			this.state.isShowNotifySuccess != nextState.isShowNotifySuccess
		)
			return true;
		return false;
	}

	componentWillReceiveProps(nextProps) {
		// update SEND ADDRESS
		let { toAddress } = nextProps;
		toAddress = toAddress.slice(0, -2);
		this.mSendAddress = toAddress;
		this.setState({
			address: toAddress,
			forceUpdate: !this.state.forceUpdate
		});
		// check valid transaction to enabled send button
		this.setState({
			isValidTransaction: this._checkIsValidTransaction()
		});

		// }
		// if change type coin
		if (this.props.typeCoin != nextProps.typeCoin) {
			this.nextTypeCoin = nextProps.typeCoin;
			this._handleOnRefresh();
		}
	}

	_handleOnGetCoinByTypeSuccess(coin) {
		console.log("TYPE : " + coin.type + " --- BALANCE : " + coin.balance);
		context.currentCoin = coin;
		// console.log("FETCH DATA SUCCESS");
		context.setState({
			currentFee: context.currentCoin.fee,
			currentRate: context.currentCoin.rate,
			currentBalance: context.currentCoin.balance,
			currentCoinType: context.currentCoin.type,
			send: 0,
			refreshing: false
		});

		context.mSend = 0;
		context.mSendMoneyInputState = SUCCESS;
		context._handleOnChangeSendMoney(context.mSend);
	}

	_getCoinByType(_type) {
		FireBase.getCoinByType(_type, this._handleOnGetCoinByTypeSuccess);
	}

	_handleOnRefresh = () => {
		// reset
		this.setState({
			send: 0,
			fee: 0,
			feeUSD: 0,
			refreshing: true,
			isShowNotifySuccess: false
		});
		this._getCoinByType(this.nextTypeCoin);
	};

	_handleOnSubmitTransaction = () => {
		this.setState({
			sending: true
		});
		this._executeTransaction();
	};

	_executeTransaction = () => {
		let { typeCoin } = this.props;
		let date = new Date();
		let timeMilis = date.getTime();

		let currentAddress = this._getCurrentAddress();
		let sendAddress = this.mSendAddress;

		console.log("Address : FROM - " + currentAddress);
		console.log("Address : TO - " + sendAddress);

		let sendTransaction = new MTransaction(
			typeCoin,
			getCurrentDate(),
			this.mSend,
			// FireBase.ACCOUNT_INFO().CURRENT_ADDRESS,
			this._getCurrentAddress(),
			this.mSendAddress,
			timeMilis
		);

		FireBase.insertSendTransactionByType(
			sendTransaction,
			typeCoin,
			() => {}
		);
		FireBase.insertInputTransactionByType(
			// inputTransaction,
			sendTransaction,
			typeCoin,
			() => {},
			this.mSendAddress
		);

		FireBase.sendMoneyFromBalance(this.mSend, typeCoin, () => {
			this.setState({
				sending: false,
				isShowNotifySuccess: true
			});
		});

		FireBase.inputMoneyToBalance(
			this.mSend,
			typeCoin,
			() => {
				this.setState({
					sending: false,
					isShowNotifySuccess: true
				});
			},
			this.mSendAddress
		);

		// let { typeCoin } = this.props;

		// setTimeout(() => {
		// 	let date = getCurrentDate();

		// 	if (typeCoin == "BITCOIN") {
		// 		let transaction = new MTransaction(
		// 			"BTC",
		// 			date,
		// 			"SEND",
		// 			this.mSend,
		// 			this.mSendAddress
		// 		);
		// 		Data.sListBitcoinTransactions.unshift(transaction);
		// 		Data.sBitcoin.balance = reduce;
		// 	}
		// 	if (typeCoin == "ETHEREUM") {
		// 		let transaction = new MTransaction(
		// 			"ETH",
		// 			date,
		// 			"SEND",
		// 			this.mSend,
		// 			this.mSendAddress
		// 		);
		// 		Data.sListEthereumTransactions.unshift(transaction);
		// 		Data.sEthereum.balance = reduce;
		// 	}
		// 	if (typeCoin == "NATO") {
		// 		let transaction = new MTransaction(
		// 			"NATO",
		// 			date,
		// 			"SEND",
		// 			this.mSend,
		// 			this.mSendAddress
		// 		);
		// 		Data.sListNatoTransactions.unshift(transaction);
		// 		Data.sNato.balance = reduce;
		// 	}
		// 	if (typeCoin == "USD") {
		// 		let transaction = new MTransaction(
		// 			"USD",
		// 			date,
		// 			"SEND",
		// 			this.mSend,
		// 			this.mSendAddress
		// 		);
		// 		Data.sListNatoTransactions.unshift(transaction);
		// 		Data.sUSD.balance = reduce;
		// 	}
		// 	if (typeCoin == "SGD") {
		// 		let transaction = new MTransaction(
		// 			"SGD",
		// 			date,
		// 			"SEND",
		// 			this.mSend,
		// 			this.mSendAddress
		// 		);
		// 		Data.sListNatoTransactions.unshift(transaction);
		// 		Data.SGD.balance = reduce;
		// 	}
		// 	if (typeCoin == "XRP") {
		// 		let transaction = new MTransaction(
		// 			"XRP",
		// 			date,
		// 			"SEND",
		// 			this.mSend,
		// 			this.mSendAddress
		// 		);
		// 		Data.sListNatoTransactions.unshift(transaction);
		// 		Data.sXRP.balance = reduce;
		// 	}
		// 	this.setState({
		// 		// feeUSD: 0,
		// 		sending: false,
		// 		isShowNotifySuccess: true
		// 	});
		// 	// this.mSend = 0;
		// 	// setTimeout(() => {
		// 	// 	this._showSendSuccessDialog();
		// 	// }, 200);
		// }, 3000);
	};

	_getCurrentAddress() {
		if (FireBase.INDEX == 0) {
			switch (this.props.typeCoin) {
				case "NATO":
					address = "0x9c44C47d78090EfFa77bAC56CC2a8e13Fe4236IF";
					break;
				case "BTC":
					address = "3Qz9HUx2MphbRjQoOG75X1GAH41fV5vFX5";
					break;
				case "ETH":
					address = "0x6c44C47d78090MfKm77bAC56CC2a8e13F77KN87";
					break;
				case "XRP":
					address = "rEb8TK3gKgk5auUkwc6sHnwrGVJH8DuaLh";
					break;
				default:
					address = "0x32be343b94f860124dc4fee278fdcbd38c102d88";
			}
		}
		if (FireBase.INDEX == 1) {
			switch (this.props.typeCoin) {
				case "NATO":
					address = "0x8c55C47d78090EfFa82bAC56CC2a8e13Fe7690IF";
					break;
				case "BTC":
					address = "3Pz9HUx2MphbTjQoOG95X1JAH41fV6YUz5";
					break;
				case "ETH":
					address = "0x6c44C87d78009MfKm77bAC19CC2a8e13F77KN87";
					break;
				case "XRP":
					address = "Kbb8TK3gKgk5auUkwc6sHnwrGVJH8Jnmyt";
					break;

				default:
					address = "0x89234278a2159b16120363bec5657a4499400db2";
					break;
			}
		}
		if (FireBase.INDEX == 2) {
			switch (this.props.typeCoin) {
				case "NATO":
					address = "0x8c65D47d78090EfFa82bAC56CD2a8e13Fe6621YU";
					break;

				case "BTC":
					address = "3Pz9HUx9NphbTjQoKG95X1JLR41fV6KMz9";
					break;
				case "ETH":
					address = "0x6c44C87d78449MfKm77bAC19CC2a8e13F77mM81";
					break;
				case "XRP":
					address = "Kbb8TK3gKgk5muUkwR6sHnwrGVJH8Jn90l";
					break;

				default:
					address = "0xafa40106ec6827ea070d696cfdc83be653076db5";
					break;
			}
		}
		if (FireBase.INDEX == 3) {
			switch (this.props.typeCoin) {
				case "NATO":
					address = "3Pz9HUx9NphbTjQoKG95X1JLR41fV6KML0";
					break;

				case "BTC":
					address = "3Pz9HUx2MphbTjQoOG95X1JAH41fV6Y98a";
					break;
				case "ETH":
					address = "0x6c44C87d78449MfKm77bAC19CC2a8e13F77m90a";
					break;
				case "XRP":
					address = "Kbb8TK3gKgk5muUkwR6sHnwrGVJH8Jn012";
					break;

				default:
					address = "0xbaq40106ec6827ea070d696cfdc83be653089sdf";
					break;
			}
		}
		if (FireBase.INDEX == 4) {
			switch (this.props.typeCoin) {
				case "NATO":
					address = "3Pz9HUx9NphbTjQoKG95X1JLR41fV6K012";
					break;

				case "BTC":
					address = "3Pz9HUx2MphbTjQoOG95X1JAH41fV6Y839";
					break;
				case "ETH":
					address = "0x6c44C87d78449MfKm77bAC19CC2a8e13F77m9as";
					break;
				case "XRP":
					address = "Kbb8TK3gKgk5muUkwR6sHnwrGVJH8Jn99l";
					break;

				default:
					address = "0xdfk40106ec6827ea070d696cfdc83be653dsf900";
					break;
			}
		}
		return address;
	}

	_showSendDialog = () => {
		Alert.alert(
			"Send",
			"Are you sure to execute transaction ?",
			[
				{
					text: "Cancel",
					style: "cancel",
					onPress: () => Keyboard.dismiss()
				},
				{
					text: "Yes",
					// onPress: () => alert("OK"),
					onPress: this._handleOnSubmitTransaction,
					// onPress: () => Keyboard.dismiss(),
					style: "cancel"
				}
			],
			{ cancelable: false }
		);
	};

	_showSendSuccessDialog = () => {
		Alert.alert(
			"Notify",
			"Execute transaction success",
			[
				{
					text: "OK",
					style: "cancel",
					onPress: () => this._handleOnRefresh()
				}
			],
			{
				cancelable: false
			}
		);
	};

	_handleOnChangeSendAddress = sendAddress => {
		this.mSendAddress = sendAddress;
		let valid = this._checkIsValidTransaction();
		this.setState({
			address: sendAddress,
			isValidTransaction: valid
		});
		// console.log("valid transaction : " + valid);
	};

	_handleOnChangeSendMoney = sendMoney => {
		this._checkMoneyIsValid(sendMoney, this.state.currentBalance);
		let valid = this._checkIsValidTransaction();

		this.setState({
			isValidTransaction: valid
		});
		// console.log("valid transaction : " + valid);
	};

	_checkIsValidTransaction = () => {
		// console.log(
		// 	`mSend : ${this.mSend} \nmSendAddress : ${this.mSendAddress}`
		// );
		if (
			this.mSend.toString().length > 0 &&
			this.mSendMoneyInputState == SUCCESS &&
			this.mSend != 0 &&
			this.mSendAddress.length > 0
		) {
			// console.log("TEST : VALID");
			return true;
		}
		// console.log("TEST : INVALID");
		return false;
	};

	_checkMoneyIsValid = (sendMoney, balance) => {
		// invalid : not number
		if (isNaN(sendMoney)) {
			this.mSendMoneyInputState = INVALID_NUMBER;
			this.setState({
				sendMoneyInputState: INVALID_NUMBER
			});
		}
		// invalid valid
		else if (sendMoney > balance) {
			this.mSendMoneyInputState = BIGGER_THAN_BALANCE;
			this.setState({
				sendMoneyInputState: BIGGER_THAN_BALANCE
			});
		}
		// valid
		else {
			this.mSend = sendMoney;
			this.mSendMoneyInputState = SUCCESS;
			this.setState({
				sendMoneyInputState: SUCCESS,
				send: sendMoney
			});
		}
	};

	_showTypeCoinDialog = () => {
		// this.popupDialog.show();
		Home._sShowTypeCoinDialog("send");
	};

	_getCurrentBalance = () => {
		// let { currentBalance, currentCoinType } = this.state;

		let currentBalance = this.currentCoin.balance;
		let currentCoinType = this.props.typeCoin;
		return `Your current balance is ${currentBalance} ${currentCoinType}`;
	};

	_getCurrentFee = () => {
		// let { send, currentFee, currentCoinType, currentRate } = this.state;
		let { send } = this.state;
		// let send = this.send;
		let currentFee = this.currentCoin.fee;
		let currentRate = this.currentCoin.rate;
		// let currentCoinType = this.currentCoin.type;
		let currentCoinType = this.props.typeCoin;
		let feeWithCurrentCoinType = this._countFee(send, currentFee);
		// console.log("FEE : " + currentCoinType);
		return `${feeWithCurrentCoinType} ${currentCoinType} ( ${(
			feeWithCurrentCoinType * currentRate
		).toFixed(1)} USD )`;
	};

	_getCurrentCoinType = coin => {
		if (coin == "BITCOIN") return "BTC";
		if (coin == "NATO") return "NATO";
		return "ETHER";
	};

	_getCurrentSendMoneyWithUSD = () => {
		let { currentFee, send, currentRate } = this.state;
		return `${(send * currentRate).toFixed(1)}`;
	};

	_countFee = (money, fee) => (money * fee).toFixed(1);

	render() {
		// console.log("RENDER");
		let { typeCoin } = this.props;
		let { refreshing, isShowNotifySuccess } = this.state;
		return (
			<ScrollView
				keyboardDismissMode={Platform.select({
					ios: "interactive",
					android: "on-drag"
				})}
				style={{ backgroundColor: "white" }}
				refreshControl={
					<RefreshControl
						refreshing={false}
						onRefresh={this._handleOnRefresh}
					/>
				}
			>
				<KeyboardAvoidingView
					keyboardVerticalOffset={80}
					behavior="position"
				>
					{this._renderHeader(typeCoin)}
					{refreshing ? (
						<LoadingView />
					) : (
						<View>
							{this._renderSendingOverlay()}
							{isShowNotifySuccess
								? this._renderSendSuccessDialog()
								: null}
							{this._renderForm()}
							{this._renderButtonSend()}
						</View>
					)}
				</KeyboardAvoidingView>
			</ScrollView>
		);
	}

	_renderSendSuccessDialog = () => (
		<CustomAlert
			visible={true}
			title={"Notify"}
			message={"Execute transaction success"}
			onPressOK={this._handleOnRefresh}
		/>
	);

	_renderSendingOverlay = () => (
		<Spinner visible={this.state.sending} cancelable={false}>
			<View
				style={{
					flex: 1,
					justifyContent: "center",
					alignItems: "center"
				}}
			>
				<View
					style={{
						backgroundColor: "white",
						justifyContent: "center",
						alignItems: "center",
						width: 300,
						height: 200,
						borderRadius: 5,
						borderWidth: 1,
						borderColor: "gray"
					}}
				>
					<Spinkit
						isVisible={true}
						// color="white"
						color="#3366CC"
						size={60}
						type={"WanderingCubes"}
					/>
					<Text
						style={{
							fontSize: 30,
							// color: "white"
							color: "#3366CC"
						}}
					>
						Sending
					</Text>
				</View>
			</View>
		</Spinner>
	);

	_renderHeader = typeCoin => (
		<View
			ref={"header"}
			style={{
				// backgroundColor: "#FFD600",
				// backgroundColor: "transparent",
				backgroundColor: "#3366CC",
				padding: 10,
				justifyContent: "center",
				alignItems: "center"
			}}
		>
			<View
				style={{
					// borderStyle:'pink',
					flexDirection: "row",
					alignItems: "center",
					justifyContent: "center",
					marginTop: 5
				}}
			>
				<Button transparent onPress={this._showTypeCoinDialog}>
					{typeCoin == "BTC" && (
						<Icon
							name="bitcoin-circle"
							type="foundation"
							size={30}
							// color={"#FFFDE7"}
							color={"#FFD600"}
						/>
					)}

					{typeCoin == "ETH" && (
						<Icon
							name="ethereum"
							type="material-community"
							size={30}
							// color={"#FFFDE7"}
							color={"#FFD600"}
						/>
					)}

					{typeCoin == "NATO" && (
						<Image
							resizeMode="stretch"
							source={require("../../../assets/image/ic_logo.png")}
							style={{ width: 30, height: 30 }}
						/>
					)}

					{typeCoin == "USD" && (
						<Image
							resizeMode="stretch"
							source={require("../../../assets/image/ic_usd.png")}
							style={{ width: 30, height: 30 }}
						/>
					)}
					{typeCoin == "EUR" && (
						<Image
							resizeMode="stretch"
							source={require("../../../assets/image/ic_euro.png")}
							style={{ width: 30, height: 30 }}
						/>
					)}
					{typeCoin == "XRP" && (
						<Image
							resizeMode="stretch"
							source={require("../../../assets/image/ic_xrp.png")}
							style={{ width: 30, height: 30 }}
						/>
					)}
					{typeCoin == "CNY" && (
						<Image
							resizeMode="stretch"
							source={require("../../../assets/image/ic_cny.png")}
							style={{ width: 30, height: 30 }}
						/>
					)}
					{typeCoin == "SGD" && (
						<Image
							resizeMode="stretch"
							source={require("../../../assets/image/ic_sgd.png")}
							style={{ width: 30, height: 30 }}
						/>
					)}

					<Text
						style={{
							// color: "#FFFDE7",
							color: "#FFD600",
							// color: "orange",
							marginLeft: 5,
							marginRight: 5,
							fontWeight: "bold",
							fontSize: 20
						}}
					>
						{typeCoin}
					</Text>
					<Icon
						name="angle-down"
						type="font-awesome"
						size={20}
						// color={"#FFFDE7"}
						color={"#FFD600"}
					/>
				</Button>
			</View>
		</View>
	);

	_renderForm = () => (
		<Form>
			<Item stackedLabel style={{ padding: 10 }}>
				<Label style={{ color: "gray" }}>From</Label>
				<Input
					style={{ fontSize: 18 }}
					placeholderTextColor="black"
					disabled
					blurOnSubmit={true}
					placeholder={`My ${this.currentCoin.name} wallet`}
				/>
			</Item>
			<Item stackedLabel style={{ padding: 10 }}>
				<Label style={{ color: "gray" }}>To</Label>
				<Input
					blurOnSubmit={true}
					ref={ref => (this.sendAddressInput = ref)}
					value={this.state.address}
					onChangeText={this._handleOnChangeSendAddress}
					placeholder={"Input your address you want to send"}
					placeholderTextColor="gray"
					style={{ fontSize: 18 }}
				/>
			</Item>
			<View style={{ flexDirection: "row", flex: 2 }}>
				<Item stackedLabel style={{ flex: 1, padding: 10 }}>
					<Label style={{ color: "gray" }}>
						{this.props.typeCoin}
					</Label>
					<Input
						ref={ref => (this.sendMoneyInput = ref)}
						keyboardType="numeric"
						onChangeText={this._handleOnChangeSendMoney}
						placeholder={"0"}
						placeholderTextColor="gray"
						style={{ fontSize: 18 }}
					/>
				</Item>
				<Item stackedLabel style={{ flex: 1, padding: 10 }}>
					<Label style={{ color: "gray" }}>USD</Label>
					<Input
						disabled
						placeholder={"0"}
						placeholderTextColor="black"
						placeholder={this._getCurrentSendMoneyWithUSD()}
						style={{ fontSize: 18 }}
					/>
				</Item>
			</View>
			{this.state.sendMoneyInputState == SUCCESS && (
				<Text
					style={{
						color: "green",
						paddingLeft: 10,
						marginLeft: 10,
						marginTop: 20
					}}
				>
					{this._getCurrentBalance()}
				</Text>
			)}
			{this.state.sendMoneyInputState == BIGGER_THAN_BALANCE && (
				<Text
					style={{
						color: "red",
						paddingLeft: 10,
						marginLeft: 10,
						marginTop: 20
					}}
				>
					send money is bigger than your balance
				</Text>
			)}
			{this.state.sendMoneyInputState == INVALID_NUMBER && (
				<Text
					style={{
						color: "red",
						paddingLeft: 10,
						marginLeft: 10,
						marginTop: 20
					}}
				>
					send money is not valid
				</Text>
			)}

			<Item stackedLabel style={{ flex: 1, padding: 10 }}>
				<Label style={{ color: "gray" }}>Fee</Label>

				<Input
					style={{ fontSize: 18 }}
					disabled
					placeholder={this._getCurrentFee()}
					placeholderTextColor="black"
				/>
			</Item>
		</Form>
	);

	_renderButtonSend = () => {
		return this.state.isValidTransaction == false ? (
			<Button
				disabled
				full
				onLayout={event => {
					var { x, y, width, height } = event.nativeEvent.layout;
					this.setState({
						heightOfSendButton: height
					});
				}}
			>
				<Text style={{ width: 300, textAlign: "center" }}>SEND</Text>
			</Button>
		) : (
			<Button info full onPress={this._showSendDialog}>
				<Text style={{ width: 300, textAlign: "center" }}>SEND</Text>
			</Button>
		);
	};
}

function mapStateToProps(state, props) {
	return {
		toAddress: state.scanQRCodeReducer.address,
		typeCoin: state.changeTypeCoinReducer.sendTab
	};
}

export default connect(
	mapStateToProps,
	null
)(SendTab);
