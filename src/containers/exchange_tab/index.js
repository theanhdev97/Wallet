import React, { Component } from "react";
import { FlatList, Dimensions, View, Alert, Image } from "react-native";
import {
	Text,
	Container,
	Title,
	Body,
	Content,
	Button,
	List,
	ListItem,
	Left,
	Right,
	ActionSheet,
	Accordion
} from "native-base";
import { Icon } from "react-native-elements";
import style from "./style";
import Home from "../home/index";
import TransactionHistory from "../../components/transaction_history/index";

// REDUX
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Action from "../../actions"; //Import your actions

class ExchangeTab extends Component {
	constructor(props) {
		super(props);
	}

	componentWillReceiveProps(nextProps) {}

	_showTypeCoinDialog = () => {
		Home._sShowTypeCoinDialog("exchange");
	};

	_getCurrentBalance = () => {
		return `${this.state.currentBalance} ${this.state.currentCoinType}`;
	};

	render() {
		let { typeCoin } = this.props;
		return (
			<Container>
				{this._renderTransactionHeader(typeCoin)}
				<TransactionHistory typeCoin={this.props.typeCoin} />
			</Container>
		);
		// {this._renderTransactionFlatList()}
	}

	_renderTransactionHeader = typeCoin => (
		// <Text
		// 	style={{ fontSize: 24, color: "#FFD600", fontWeight: "bold" }}
		// >
		// 	{this._getCurrentBalance()}
		// </Text>
		<View
			style={{
				// backgroundColor: "#FFD600",
				// backgroundColor: "transparent",
				backgroundColor: "#3366CC",
				padding: 10,
				justifyContent: "center",
				alignItems: "center"
			}}
		>
			<View
				style={{
					// borderStyle:'pink',
					flexDirection: "row",
					alignItems: "center",
					justifyContent: "center",
					marginTop: 5
				}}
			>
				<Button transparent onPress={this._showTypeCoinDialog}>
					{typeCoin == "BTC" && (
						<Icon
							name="bitcoin-circle"
							type="foundation"
							size={30}
							// color={"#FFFDE7"}
							color={"#FFD600"}
						/>
					)}

					{typeCoin == "ETH" && (
						<Icon
							name="ethereum"
							type="material-community"
							size={30}
							// color={"#FFFDE7"}
							color={"#FFD600"}
						/>
					)}

					{typeCoin == "NATO" && (
						<Image
							resizeMode="stretch"
							source={require("../../../assets/image/ic_logo.png")}
							style={{ width: 30, height: 30 }}
						/>
					)}
					{typeCoin == "USD" && (
						<Image
							resizeMode="stretch"
							source={require("../../../assets/image/ic_usd.png")}
							style={{ width: 30, height: 30 }}
						/>
					)}
					{typeCoin == "EUR" && (
						<Image
							resizeMode="stretch"
							source={require("../../../assets/image/ic_euro.png")}
							style={{ width: 30, height: 30 }}
						/>
					)}
					{typeCoin == "XRP" && (
						<Image
							resizeMode="stretch"
							source={require("../../../assets/image/ic_xrp.png")}
							style={{ width: 30, height: 30 }}
						/>
					)}
					{typeCoin == "CNY" && (
						<Image
							resizeMode="stretch"
							source={require("../../../assets/image/ic_cny.png")}
							style={{ width: 30, height: 30 }}
						/>
					)}
					{typeCoin == "SGD" && (
						<Image
							resizeMode="stretch"
							source={require("../../../assets/image/ic_sgd.png")}
							style={{ width: 30, height: 30 }}
						/>
					)}

					<Text
						style={{
							// color: "#FFFDE7",
							color: "#FFD600",
							marginLeft: 5,
							marginRight: 5,
							fontWeight: "bold",
							fontSize: 20
						}}
					>
						{typeCoin}
					</Text>
					<Icon
						name="angle-down"
						type="font-awesome"
						size={20}
						// color={"#FFFDE7"}
						color={"#FFD600"}
					/>
				</Button>
			</View>
		</View>
	);
}

function mapStateToProps(state, props) {
	return {
		typeCoin: state.changeTypeCoinReducer.exchangeTab
	};
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators(Action, dispatch);
}

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(ExchangeTab);
