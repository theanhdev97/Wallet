import { combineReducers } from "redux";
import {
	scanQRCodeReducer,
	changeTypeCoinReducer,
	changeTitleHomeReducer,
	coinReducer
} from "./reducers";

// Combine all the reducers
const rootReducer = combineReducers({
	// dataReducer
	scanQRCodeReducer,
	changeTypeCoinReducer,
	changeTitleHomeReducer,
	coinReducer
});

export default rootReducer;
