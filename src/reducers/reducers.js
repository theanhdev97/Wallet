import {
	SCAN_QR_CODE,
	CHANGE_COIN_TYPE,
	CHANGE_TITLE_HOME,
	FETCH_COIN_DATA,
	FETCH_BITCOIN_TRANSACTION,
	CHANGE_TOTAL_BALANCE
} from "../actions/action_types";

let qrCodeState = {
	address: ""
};

export const scanQRCodeReducer = (state = qrCodeState, action) => {
	switch (action.type) {
		case SCAN_QR_CODE:
			state = {
				address: action.data
			};
		default:
			return state;
	}
};

let typeCoinState = {
	sendTab: "NATO",
	exchangeTab: "NATO",
	requestTab: "NATO"
};

export const changeTypeCoinReducer = (state = typeCoinState, action) => {
	if (action.type == CHANGE_COIN_TYPE)
		switch (action.data.screen) {
			case "send":
				return {
					...state,
					sendTab: action.data.typeCoin
				};
			case "exchange":
				return {
					...state,
					exchangeTab: action.data.typeCoin
				};
			case "request":
				return {
					...state,
					requestTab: action.data.typeCoin
				};
			default:
				return state;
		}
	return state;
};

let titleHomeState = {
	title: "Dashboard"
};

export const changeTitleHomeReducer = (state = titleHomeState, action) => {
	if (action.type == CHANGE_TITLE_HOME)
		return {
			title: action.data
		};
	return state;
};

let coinState = {
	totalBalance: "",
	bitcoinRate: 0,
	bitcoinBalance: 0,
	bitcoinFee: 0,
	bitcoinTransactions: [],
	ethereumRate: 0,
	ethereumBalance: 0,
	ethereumFee: 0,
	ethereumTransactions: [],
	natoRate: 0,
	natoBalance: 0,
	natoFee: 0,
	natoTransactions: []
};

export const coinReducer = (state = coinState, action) => {
	switch (action.type) {
		case FETCH_COIN_DATA:
			return Object.assign({}, state, action.data);
		case FETCH_BITCOIN_TRANSACTION:
			return Object.assign({}, state, {
				bitcoinTransactions: action.data
			});
		case CHANGE_TOTAL_BALANCE:
			console.log("Reducer : total balance :" + action.data);
			return Object.assign({}, state, {
				totalBalance: action.data
			});
		default:
			return state;
	}
};
