export const SCAN_QR_CODE = 'scan_qr_code';
export const CHANGE_COIN_TYPE = 'change_coin_type';
export const CHANGE_TITLE_HOME = 'change_title_home';
export const FETCH_COIN_DATA = 'fetch_coin_data'; 
export const FETCH_BITCOIN_TRANSACTION = 'fetch_bitcoin_transaction';
export const CHANGE_TOTAL_BALANCE = 'change_total_balance';