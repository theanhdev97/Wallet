import {
	SCAN_QR_CODE,
	CHANGE_COIN_TYPE,
	CHANGE_TITLE_HOME,
	FETCH_COIN_DATA,
	FETCH_BITCOIN_TRANSACTION,
	CHANGE_TOTAL_BALANCE
} from "./action_types";

import MTransaction from "../model/MTransaction";

export function scanQRCode(address) {
	return {
		type: SCAN_QR_CODE,
		data: address
	};
}

export function changeCoinType(screen, type) {
	return {
		type: CHANGE_COIN_TYPE,
		data: {
			screen: screen,
			typeCoin: type
		}
	};
}

export function changeTitleHome(title) {
	return {
		type: CHANGE_TITLE_HOME,
		data: title
	};
}

export function fetchCoinData() {
	return {
		type: FETCH_COIN_DATA,
		data: {
			bitcoinRate: 751.234,
			bitcoinBalance: 100,
			bitcoinFee: 0.01,
			ethereumRate: 58.23,
			ethereumBalance: 120,
			ethereumFee: 0.05,
			natoRate: 1.105,
			natoBalance: 1000,
			natoFee: 0.025
		}
	};
}

export function fetchBitcoinTransactions() {
	let bitcoinTransactions = [];
	bitcoinTransactions.push(
		new MTransaction(
			"BTC",
			"January 31",
			"RECEIVED",
			"120.02",
			"0x50123f651298ff160b438320e09fe58c513ba56c"
		)
	);

	bitcoinTransactions.push(
		new MTransaction(
			"BTC",
			"January 30",
			"RECEIVED",
			"230.02",
			"0x21323f651298ff160b438320e09fe58c513ba34s"
		)
	);

	bitcoinTransactions.push(
		new MTransaction(
			"BTC",
			"January 30",
			"SEND",
			"1233.02",
			"0x21323f651298ff160b438320e09fe58c513ba34s"
		)
	);

	return {
		type: FETCH_BITCOIN_TRANSACTION,
		data: bitcoinTransactions
	};
}

export function changeTotalBalance(money) {
	return {
		type: CHANGE_TOTAL_BALANCE,
		data: money
	};
}
