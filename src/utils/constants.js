export const BOTTOM_TAB = [
	{
		key: "dashboard",
		icon: "home",
		label: "Bảng điều khiển",
		barColor: "#white",
		pressColor: "gray",

		activeColor: "#FFD600",
		unActiveColor: "gray",
		type: "font-awesome"
	},
	{
		key: "send",
		icon: "send",
		label: "Gửi",
		barColor: "#white",
		activeColor: "#FFD600",
		unActiveColor: "gray",
		pressColor: "gray",
		type: "material-community"
	},

	{
		key: "transaction",
		icon: "credit-card",
		label: "Giao dịch",
		barColor: "#white",
		pressColor: "gray",
		activeColor: "#FFD600",
		unActiveColor: "gray",
		type: "font-awesome"
	},
	{
		key: "request",
		icon: "download",
		label: "Yêu cầu",
		barColor: "#white",
		activeColor: "#FFD600",
		unActiveColor: "gray",
		pressColor: "gray",
		type: "font-awesome"
	}
];

export const MENUS = [
	{
		icon: {
			name: "login",
			type: "material-community"
		},
		name: "Go to exchange"
	},
	{
		icon: {
			name: "history",
			type: "font-awesome"

			// type: "font-awesome"
		},
		name: "History"
	},
	{
		icon: {
			name: "calendar-range",
			type: "material-community"
		},
		name: "Event"
	},
	{
		icon: {
			name: "question-circle",
			type: "font-awesome"
		},
		name: "Support"
	},
	{
		icon: {
			name: "cog",
			type: "font-awesome"
		},
		name: "Settings"
	},
	{
		icon: {
			name: "info",
			type: "font-awesome"
		},
		name: "About us"
	},
	{
		icon: {
			name: "arrow-left",
			type: "font-awesome"
		},
		name: "Log out"
	}
];

export const TYPE_COINS = [
	"NATO",
	"USD",
	"SGD",
	"BTC",
	"ETH",
	"XRP",
	"EUR",
	"CNY"
];
