import firebase from "react-native-firebase";
import MCoin from "../model/MCoin";
import MTransaction from "../model/MTransaction";

// const ACC_1 = true;
// const ACC_1 = false;

const ACC_1_USERNAME = "adminnatoexchange@gmail.com";
const ACC_1_PASSWORD = "admin123";
const ACC_1_INDEX = 0;

const ACC_2_USERNAME = "ricky@gmail.com";
const ACC_2_PASSWORD = "ricky123";
const ACC_2_INDEX = 1;

const ACC_3_USERNAME = "lisa@gmail.com";
const ACC_3_PASSWORD = "lisa123";
const ACC_3_INDEX = 2;

const ACC_4_USERNAME = "tony@gmail.com";
const ACC_4_PASSWORD = "tony123";
const ACC_4_INDEX = 3;

const ACC_5_USERNAME = "steve@gmail.com";
const ACC_5_PASSWORD = "steve123";
const ACC_5_INDEX = 4;

class FireBase {
	// 0 => b1
	// 1 => b2
	// 2 => b2
	static INDEX = 0;

	static loginAccount = (userName, password) => {
		if (userName == ACC_1_USERNAME && password == ACC_1_PASSWORD) {
			FireBase.INDEX = ACC_1_INDEX;
			return true;
		}
		if (userName == ACC_2_USERNAME && password == ACC_2_PASSWORD) {
			FireBase.INDEX = ACC_2_INDEX;
			return true;
		}
		if (userName == ACC_3_USERNAME && password == ACC_3_PASSWORD) {
			FireBase.INDEX = ACC_3_INDEX;
			return true;
		}
		if (userName == ACC_4_USERNAME && password == ACC_4_PASSWORD) {
			FireBase.INDEX = ACC_4_INDEX;
			return true;
		}
		if (userName == ACC_5_USERNAME && password == ACC_5_PASSWORD) {
			FireBase.INDEX = ACC_5_INDEX;
			return true;
		}
		return false;
	};

	static getAnotherAddress() {
		if ((FireBase.INDEX = ACC_1_INDEX))
			return "0x32be343b94f860124dc4fee278fdcbd38c102d88";
		if ((FireBase.INDEX = ACC_2_INDEX))
			return "0x89234278a2159b16120363bec5657a4499400db2";
		if ((FireBase.INDEX = ACC_3_INDEX))
			return "0xafa40106ec6827ea070d696cfdc83be653076db5";
		if ((FireBase.INDEX = ACC_4_INDEX))
			return "0xbaq40106ec6827ea070d696cfdc83be653089sdf";
		if ((FireBase.INDEX = ACC_5_INDEX))
			return "0xdfk40106ec6827ea070d696cfdc83be653dsf900";
	}

	static getRootOnFireBaseByAddress = address => {
		if (
			address == "0x9c44C47d78090EfFa77bAC56CC2a8e13Fe4236IF" ||
			address == "3Qz9HUx2MphbRjQoOG75X1GAH41fV5vFX5" ||
			address == "0x6c44C47d78090MfKm77bAC56CC2a8e13F77KN87" ||
			address == "0x32be343b94f860124dc4fee278fdcbd38c102d88" ||
			address == "rEb8TK3gKgk5auUkwc6sHnwrGVJH8DuaLh"
		)
			return "b1";
		if (
			address == "0x8c55C47d78090EfFa82bAC56CC2a8e13Fe7690IF" ||
			address == "3Pz9HUx2MphbTjQoOG95X1JAH41fV6YUz5" ||
			address == "0x6c44C87d78009MfKm77bAC19CC2a8e13F77KN87" ||
			address == "0x89234278a2159b16120363bec5657a4499400db2" ||
			address == "Kbb8TK3gKgk5auUkwc6sHnwrGVJH8Jnmyt"
		)
			return "b2";
		if (
			address == "0x8c65D47d78090EfFa82bAC56CD2a8e13Fe6621YU" ||
			address == "3Pz9HUx9NphbTjQoKG95X1JLR41fV6KMz9" ||
			address == "0x6c44C87d78449MfKm77bAC19CC2a8e13F77mM81" ||
			address == "0xafa40106ec6827ea070d696cfdc83be653076db5" ||
			address == "Kbb8TK3gKgk5muUkwR6sHnwrGVJH8Jn90l"
		)
			return "b3";
		if (
			address == "0xbaq40106ec6827ea070d696cfdc83be653089sdf" ||
			address == "3Pz9HUx9NphbTjQoKG95X1JLR41fV6KML0" ||
			address == "3Pz9HUx2MphbTjQoOG95X1JAH41fV6Y98a" ||
			address == "0x6c44C87d78449MfKm77bAC19CC2a8e13F77m90a" ||
			address == "Kbb8TK3gKgk5muUkwR6sHnwrGVJH8Jn012"
		)
			return "b4";
		if (
			address == "0xdfk40106ec6827ea070d696cfdc83be653dsf900" ||
			address == "3Pz9HUx9NphbTjQoKG95X1JLR41fV6K012" ||
			address == "3Pz9HUx2MphbTjQoOG95X1JAH41fV6Y839" ||
			address == "0x6c44C87d78449MfKm77bAC19CC2a8e13F77m9as" ||
			address == "Kbb8TK3gKgk5muUkwR6sHnwrGVJH8Jn99l"
		)
			return "b5";
	};

	static ACCOUNT_INFO() {
		switch (FireBase.INDEX) {
			case ACC_1_INDEX:
				return {
					CURRENT_ACCOUNT: "b1",
					CURRENT_ADDRESS:
						"0x32be343b94f860124dc4fee278fdcbd38c102d88",
					CURRENT_ADDRESS_1:
						"0x9c44C47d78090EfFa77bAC56CC2a8e13Fe4236IF",
					CURRENT_ADDRESS_2: "3Qz9HUx2MphbRjQoOG75X1GAH41fV5vFX5",
					CURRENT_ADDRESS_3:
						"0x6c44C47d78090MfKm77bAC56CC2a8e13F77KN87",
					CURRENT_ADDRESS_4: "rEb8TK3gKgk5auUkwc6sHnwrGVJH8DuaLh"
				};
			case ACC_2_INDEX:
				return {
					CURRENT_ACCOUNT: "b2",
					CURRENT_ADDRESS:
						"0x89234278a2159b16120363bec5657a4499400db2",
					CURRENT_ADDRESS_1:
						"0x8c55C47d78090EfFa82bAC56CC2a8e13Fe7690IF",
					CURRENT_ADDRESS_2: "3Pz9HUx2MphbTjQoOG95X1JAH41fV6YUz5",
					CURRENT_ADDRESS_3:
						"0x6c44C87d78009MfKm77bAC19CC2a8e13F77KN87",
					CURRENT_ADDRESS_4: "Kbb8TK3gKgk5auUkwc6sHnwrGVJH8Jnmyt"
				};
			case ACC_3_INDEX:
				return {
					CURRENT_ACCOUNT: "b3",
					CURRENT_ADDRESS:
						"0xafa40106ec6827ea070d696cfdc83be653076db5",
					CURRENT_ADDRESS:
						"0x8c65D47d78090EfFa82bAC56CD2a8e13Fe6621YU",
					CURRENT_ADDRESS_1: "3Pz9HUx9NphbTjQoKG95X1JLR41fV6KMz9",
					CURRENT_ADDRESS_2: "3Pz9HUx2MphbTjQoOG95X1JAH41fV6YUz5",
					CURRENT_ADDRESS_3:
						"0x6c44C87d78449MfKm77bAC19CC2a8e13F77mM81",
					CURRENT_ADDRESS_4: "Kbb8TK3gKgk5muUkwR6sHnwrGVJH8Jn90l"
				};
			case ACC_4_INDEX:
				return {
					CURRENT_ACCOUNT: "b4",
					CURRENT_ADDRESS:
						"0xbaq40106ec6827ea070d696cfdc83be653089sdf",
					CURRENT_ADDRESS_1: "3Pz9HUx9NphbTjQoKG95X1JLR41fV6KML0",
					CURRENT_ADDRESS_2: "3Pz9HUx2MphbTjQoOG95X1JAH41fV6Y98a",
					CURRENT_ADDRESS_3:
						"0x6c44C87d78449MfKm77bAC19CC2a8e13F77m90a",
					CURRENT_ADDRESS_4: "Kbb8TK3gKgk5muUkwR6sHnwrGVJH8Jn012"
				};
			case ACC_5_INDEX:
				return {
					CURRENT_ACCOUNT: "b5",
					CURRENT_ADDRESS:
						"0xdfk40106ec6827ea070d696cfdc83be653dsf900",
					CURRENT_ADDRESS_1: "3Pz9HUx9NphbTjQoKG95X1JLR41fV6K012",
					CURRENT_ADDRESS_2: "3Pz9HUx2MphbTjQoOG95X1JAH41fV6Y839",
					CURRENT_ADDRESS_3:
						"0x6c44C87d78449MfKm77bAC19CC2a8e13F77m9as",
					CURRENT_ADDRESS_4: "Kbb8TK3gKgk5muUkwR6sHnwrGVJH8Jn99l"
				};
			default:
				return;
		}
	}

	static getListCoins(_handleSuccess) {
		let data = firebase
			.database()
			.ref()
			.child(`/${FireBase.ACCOUNT_INFO().CURRENT_ACCOUNT}/coins`);
		let coins = [];

		data.on("value", snapshot => {
			console.log("Real time : getListCoins()");
			let coins = [];
			let json = snapshot.toJSON();
			for (k in json) {
				let c = json[`${k}`];
				let coin = new MCoin(
					c.name,
					c.balance,
					c.rate,
					c.volume,
					c.orientation,
					c.type,
					c.color,
					c.fee
				);
				coins.push(coin);
			}
			_handleSuccess(coins);
		});
	}

	static getCoinByType(_type, _handleSuccess) {
		let data = firebase
			.database()
			.ref()
			.child(`/${FireBase.ACCOUNT_INFO().CURRENT_ACCOUNT}/coins`);

		data.on("value", snapshot => {
			let json = snapshot.toJSON();
			for (k in json) {
				let c = json[`${k}`];
				if (c.type == _type) {
					let coin = new MCoin(
						c.name,
						parseFloat(c.balance),
						parseFloat(c.rate),
						parseFloat(c.volume),
						parseFloat(c.orientation),
						c.type,
						c.color,
						parseFloat(c.fee)
					);
					_handleSuccess(coin);
					return;
				}
			}
		});
	}

	static getTransactionByType(_type, _handleSuccess) {
		let data = firebase
			.database()
			.ref()
			.child(
				`/${
					FireBase.ACCOUNT_INFO().CURRENT_ACCOUNT
				}/transactions/${_type}`
			);

		data.on("value", snapshot => {
			let json = snapshot.toJSON();
			let arr = [];
			for (let k in json) {
				let i = json[`${k}`];
				let mTransaction = new MTransaction(
					i.coinType,
					i.time,
					i.money,
					i.fromAddress,
					i.toAddress,
					i.timeMilis
				);
				arr.push(mTransaction);
			}

			_handleSuccess(arr);
		});
	}

	static compareDate(date1, date2) {
		var parts = date1.split("-");
		var d1 = Number(parts[2] + parts[1] + parts[0]);
		parts = date2.split("-");
		var d2 = Number(parts[2] + parts[1] + parts[0]);
		return d1 > d2;
	}

	static insertSendTransactionByType(_transaction, _coinType, _callback) {
		let ref = firebase
			.database()
			.ref()
			.child(
				`/${
					FireBase.ACCOUNT_INFO().CURRENT_ACCOUNT
				}/transactions/${_coinType}`
			);
		ref.push(_transaction, _callback);
	}

	static insertInputTransactionByType(
		_transaction,
		_coinType,
		_callback,
		_address
	) {
		let ref = firebase
			.database()
			.ref()
			.child(
				`/${FireBase.getRootOnFireBaseByAddress(
					_address
				)}/transactions/${_coinType}`
			);
		ref.push(_transaction, _callback);
	}

	static sendMoneyFromBalance(_send, _coinType, _callback) {
		let isFirst = true;
		let ref = firebase
			.database()
			.ref()
			.child(
				`/${FireBase.ACCOUNT_INFO().CURRENT_ACCOUNT}/coins/${_coinType}`
			);
		ref.once("value", dataSnapshot => {
			let json = dataSnapshot.toJSON();
			let currentBalance = json.balance - _send;
			if (isFirst) {
				ref.update({
					balance: currentBalance
				});
				isFirst = false;
				_callback();
			}
		});
	}

	static inputMoneyToBalance(_input, _coinType, _callback, _address) {
		let isFirst = true;
		let ref = firebase
			.database()
			.ref()
			.child(
				`/${FireBase.getRootOnFireBaseByAddress(
					_address
				)}/coins/${_coinType}`
			);
		ref.once("value", dataSnapshot => {
			let json = dataSnapshot.toJSON();
			let currentBalance = parseFloat(json.balance) + parseFloat(_input);
			console.log("Current balance : " + currentBalance);
			if (isFirst) {
				isFirst = false;
				ref.update({
					balance: currentBalance
				});
				_callback();
			}
		});
	}
}

export default FireBase;
